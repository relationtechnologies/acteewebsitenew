﻿using System;
using System.Configuration;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Scheduling;

namespace Actee.Web.Components
{
    public class BackgroundTaskComposer
        : ComponentComposer<BackgroundTaskComponent>
    { }

    public class BackgroundTaskComponent : IComponent
    {
        private readonly IProfilingLogger logger;
        private readonly IRuntimeState runtimeState;
        private readonly IContentService contentService;
        private readonly IUmbracoContextFactory umbracoContextFactory;
        public BackgroundTaskComponent(IRuntimeState runtimeState, IProfilingLogger logger, IContentService contentService, IUmbracoContextFactory umbracoContextFactory)
        {
            this.runtimeState = runtimeState;
            this.logger = logger;
            this.contentService = contentService;
            this.umbracoContextFactory = umbracoContextFactory;
        }

        public void Initialize()
        {
            var delayMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["FirstLoad"]);
            var periodMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["Interval"]);
            // time in number * miliseconds = minutes
            var delay = delayMinutes * 60000;  
            var period = periodMinutes * 60000; 

            // register background task
            var runner = new BackgroundTaskRunner<IBackgroundTask>("Custom Task", logger);
            if (runner != null)
            {
                var check = new CustomBackgroundTask(runner, delay, period, logger, runtimeState, contentService, umbracoContextFactory);
                runner.Add(check);
            }
        }

        public void Terminate()
        {
            // umbraco closing down
        }
    }
}