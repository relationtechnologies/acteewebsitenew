﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using Umbraco.Core.Sync;
using Umbraco.Web;
using Umbraco.Web.Scheduling;

namespace Actee.Web.Components
{
    public class CustomBackgroundTask : RecurringTaskBase
    {
        private readonly IRuntimeState runtimeState;
        private readonly IProfilingLogger logger;
        private readonly IContentService _contentService;
        private readonly IUmbracoContextFactory _umbracoContextFactory;
        public CustomBackgroundTask(
            IBackgroundTaskRunner<RecurringTaskBase> runner, int delayMilliseconds, int periodMilliseconds,
            IProfilingLogger logger, IRuntimeState runtimeState, IContentService contentService, IUmbracoContextFactory umbracoContextFactory)
            : base(runner, delayMilliseconds, periodMilliseconds)
        {
            this.runtimeState = runtimeState;
            this.logger = logger;
            _contentService = contentService;
            _umbracoContextFactory = umbracoContextFactory;
        }
        public override bool IsAsync => true;


        public override Task<bool> PerformRunAsync(CancellationToken token)
        {
            if (RunOnThisServer())
            {
                logger.Info<CustomBackgroundTask>("Running our task");

                long chid;
                var allGamePage = _contentService.GetPagedChildren(_contentService.GetRootContent().FirstOrDefault().Id, 0, 100, out chid).FirstOrDefault(x => x.ContentType.Alias.Equals("games"));
                if (allGamePage != null)
                {
                    Actee.Web.Helpers.Extensions.GetAndAddAllGamesinBackend(allGamePage.Id, _contentService, _umbracoContextFactory);
                    Helpers.Extensions.GetAndAddAllGamesinBackendForQuiz(allGamePage.Id, _contentService, _umbracoContextFactory);
                    Helpers.Extensions.GetAndAddAllGamesinBackendForStyletrade(allGamePage.Id, _contentService, _umbracoContextFactory);
                }
            };

            // technically we shoud run things async for performances
            // because we are doing nothing here, just return the task.

            // returning true if you want your task to run again, false if you don't
            return Task.FromResult<bool>(true);
        }


        private bool RunOnThisServer()
        {
            switch (runtimeState.ServerRole)
            {
                case ServerRole.Replica:
                    logger.Debug<CustomBackgroundTask>("Not running on a replica");
                    return false;
                case ServerRole.Unknown:
                    logger.Debug<CustomBackgroundTask>("Not running on a unknown server role");
                    return false;
                // case ServerRole.Master:
                // case ServerRole.Single:
                default:
                    logger.Debug<CustomBackgroundTask>("Running on master or single");
                    return true;
            }
        }
    }
}