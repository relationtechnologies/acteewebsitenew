﻿using Actee.Web.Helpers;
using Actee.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Mvc;


namespace Actee.Web.Controllers
{
    public class BlogController : SurfaceController
    {
        // private readonly IMediaService _mediaService;
        private readonly IContentTypeBaseServiceProvider _contentTypeBaseServiceProvider;
        public BlogController(IContentTypeBaseServiceProvider contentTypeBaseServiceProvider)
        {
            _contentTypeBaseServiceProvider = contentTypeBaseServiceProvider;
        }


        [HttpPost]
        public ActionResult GetBlogPosts(int id, int skipRecords, int noOfRecords, string filter)
        {
            Blogs model = new Blogs();
            model.blogposts = Extensions.GetBlogs(Umbraco.Content(id), skipRecords, noOfRecords, filter);

            return PartialView("~/Views/Partials/BlogLoadMore.cshtml", model);
        }

        private HttpWebRequest httpWebRequest;

        public string AddNewCountry(Country country)
        {
            try
            {
                var contentService = Services.ContentService;
                long children;
                var existNode = contentService.GetPagedChildren(2823, 0, 1000, out children).Where(x => x.GetValue<string>("locationName", "en-US").Equals(country.name)).FirstOrDefault();
                if (existNode == null)
                {
                    var BlogData = contentService.Create(country.name, 2823, "location");
                    if (country.name != "" && country.name != null)
                        BlogData.SetValue("locationName", country.name, "en-US");
                    BlogData.SetCultureName(country.name, "en-US");
                    contentService.SaveAndPublish(BlogData, "en-US");
                    // umbraco.library.RefreshContent();
                    return BlogData.Id.ToString();
                }
                else
                {
                    return existNode.Id.ToString();
                }


            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        [HttpPost]
        public string AddEmployeeProfile(Employee model)
        {
            string error = "0";
            try
            {
                if (model != null)
                {
                    error = "1";
                    var contentService = Services.ContentService;
                    error = "1.1";
                    if (model.ActionType.ToLower() == "create" || model.ActionType.ToLower() == "update")
                    {
                        error = "2";
                        IContent BlogData = null;
                        error = "2.1";
                        if (model.ActionType.ToLower() == "create")
                        {
                            var consultantsPageId = Convert.ToInt32(ConfigurationManager.AppSettings["ConsultantsPageId"]);
                            BlogData = contentService.Create(model.EmployeeName, consultantsPageId, "partnerCards");
                            BlogData.Key = new Guid();
                        }
                        else
                        {
                            BlogData = contentService.GetById(model.NodeId);
                            BlogData.Name = (model.EmployeeName != "" && model.EmployeeName != null) ? model.EmployeeName : BlogData.Name;
                        }
                        BlogData.SetCultureName(model.EmployeeName, "en-US");
                        List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
                        error = "3";
                        if (model.SocialMediaList != null && model.SocialMediaList.Count() > 0)
                        {
                            foreach (var item in model.SocialMediaList)
                            {
                                string mediaid = "";
                                var guidimage = "7b6ab2b5-735e-49cc-b77f-ecc0c2f63fac";

                                if (item.Name.ToLower().Contains("link"))
                                {
                                    guidimage = "7b6ab2b5-735e-49cc-b77f-ecc0c2f63fac";
                                }
                                else if (item.Name.ToLower().Contains("mail"))
                                {
                                    guidimage = "e25bc393-1770-4c90-8321-adb28020ec8c";
                                }

                                mediaid = string.Concat("umb://media/", guidimage.Replace("-", ""));

                                items.Add(new Dictionary<string, object>() {
                                    { "key", new Guid() },
                                    { "name", item.Name },
                                    { "ncContentTypeAlias", "socialMediaNC"},
                                    { "socialMediaName" , item.Name},
                                    { "iconImage", mediaid },
                                    { "iconLink", item.IconLink },
                                    { "iconHeading", item.IconHeading},
                                    { "iconDescription", item.IconDescription}
                                });
                            }
                        }

                        List<string> otherCategoriesList = new List<string>();

                        error = "4";
                        if (model.EmployeeType != null)
                        {
                            foreach (var item in model.EmployeeType)
                            {
                                long count;
                                //var blogcat = contentService.GetChildrenByName(1407, item);
                                var blogcat = contentService.GetPagedChildren(1407, 0, 1000, out count);
                                if (blogcat.Count() <= 0)
                                {
                                    blogcat = contentService.GetPagedChildren(3285, 0, 1000, out count);  //contentService.GetChildrenByName(3285, item);
                                }
                                foreach (var cat in blogcat)
                                {
                                    var guid = contentService.GetById(cat.Id).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }
                            }
                        }

                        int countryId = 0;

                        error = "5";
                        if (model.CountriesOfOperation != null)
                        {
                            foreach (var item in model.CountriesOfOperation)
                            {
                                long count2;
                                //var countries = contentService.GetById(3194).Children;
                                var countries = contentService.GetPagedChildren(2960, 0, 1000, out count2);
                                int isCountryExist = 0;
                                int catId = 0;
                                if (countries.Count() > 0)
                                {
                                    foreach (var cat in countries)
                                    {
                                        if (cat.Name == item.name)
                                        {
                                            catId = cat.Id;
                                            isCountryExist = 1;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    Country country = new Country();
                                    country.name = item.name ?? string.Empty;
                                    country.code = item.code ?? string.Empty;
                                    var guid = contentService.GetById(int.Parse(AddNewCountry(country))).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }

                                if (isCountryExist == 1)
                                {
                                    var guid = contentService.GetById(catId).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }
                                else
                                {
                                    Country country = new Country();
                                    country.name = item.name;
                                    country.code = item.code;

                                    countryId = int.Parse(AddNewCountry(country));

                                    var guid = contentService.GetById(countryId).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }
                            }
                        }

                        string ConsultantType = "";

                        error = "6";
                        //if (!string.IsNullOrEmpty(model.ConsultantType))
                        //{
                        //    var yourPropertyPrevaluesRootIterator = umbraco.library.GetPreValues(3305);
                        //    yourPropertyPrevaluesRootIterator.MoveNext();
                        //    var yourPropertyPrevaluesIterator = yourPropertyPrevaluesRootIterator.Current.SelectChildren("preValue", "");
                        //    Dictionary<string, string> yourPropertyPrevalues = new Dictionary<string, string>();

                        //    while (yourPropertyPrevaluesIterator.MoveNext())
                        //    {
                        //        yourPropertyPrevalues.Add(yourPropertyPrevaluesIterator.Current.Value, yourPropertyPrevaluesIterator.Current.GetAttribute("id", ""));
                        //    }

                        //    string checkItemsIds = "";

                        //    if (model.ConsultantType != "" && model.ConsultantType != null)
                        //    {
                        //        string[] arrCT = model.ConsultantType.Split(',');
                        //        foreach (var ct in arrCT)
                        //        {
                        //            if (ct.ToLower() == "internal")
                        //                checkItemsIds += yourPropertyPrevalues["Internal"] + ",";
                        //            else
                        //                checkItemsIds += yourPropertyPrevalues["External"] + ",";
                        //        }
                        //    }

                        //    char[] noarr = checkItemsIds.ToCharArray();

                        //    if (noarr[noarr.Length - 1] == ',')
                        //    {
                        //        StringBuilder sb = new StringBuilder(checkItemsIds);
                        //        sb.Remove((noarr.Length - 1), 1);
                        //        ConsultantType = sb.ToString();
                        //    }
                        //}

                        List<Dictionary<string, object>> conceptsList = new List<Dictionary<string, object>>();
                        if (model.ConceptCertifiedIn != null)
                        {
                            foreach (var conce in model.ConceptCertifiedIn)
                            {
                                if (conce.IsApproved)
                                {
                                    conceptsList.Add(new Dictionary<string, object>() {
                                        { "key", new Guid() },
                                        { "name", conce.name },
                                        { "ncContentTypeAlias", "conceptCertifiedNC"},
                                        { "conceptName", conce.name},
                                        { "value", conce.value},
                                        { "instructorName", conce.instructorName},
                                        { "certificationDate", conce.certificationDate},
                                        { "placeOfCertification", conce.placeOfCertification},
                                        { "approvedDate", conce.approvedDate}
                                    });
                                }
                            }
                        }

                        DateTime dateTime = new DateTime();

                        if (model.DateOfCertification != null)
                        {
                            dateTime = DateTime.Parse(model.DateOfCertification);
                        }

                        string[] image = model.EmployeeSmallImage != "" && model.EmployeeSmallImage != null ? model.EmployeeSmallImage.Split('/') : null;

                        var languageCode = "en-Us";
                        if (model.EmployeeName != "" && model.EmployeeName != null)
                        {
                            BlogData.Name = model.EmployeeName;
                            BlogData.SetValue("partnerName", model.EmployeeName, languageCode);
                        }
                        if (model.Desgination != "" && model.Desgination != null)
                            BlogData.SetValue("partnerAddress", model.Desgination, languageCode);
                        if (model.ContactNumber != "" && model.ContactNumber != null)
                            // BlogData.SetValue("contactNumber", model.ContactNumber, languageCode);

                            if (model.EmployeeSmallImage != "" && model.EmployeeSmallImage != null)
                            {
                                var mediaNodeId = Convert.ToInt32(ConfigurationManager.AppSettings["MediaNodeId"]);
                                GuidUdi guidUdi = DownloadImage(model.EmployeeSmallImage, image[image.Length - 1], mediaNodeId);
                                if (guidUdi != null)
                                {
                                    BlogData.SetValue("partnersImage", guidUdi.ToString(), languageCode);
                                }

                            }



                        if (items.Count() > 0 && items != null)
                            //BlogData.SetValue("socialMediaList", JsonConvert.SerializeObject(items), languageCode);
                            if (model.EmployeeDescription != "" && model.EmployeeDescription != null)
                                BlogData.SetValue("partnerDescription", model.EmployeeDescription, languageCode);
                        // if (otherCategoriesList.Count() > 0 && otherCategoriesList != null)
                        BlogData.SetValue("partnerType", "Consultant", languageCode);
                        //if (model.ConsultantType != "" && model.ConsultantType != null)
                        //BlogData.SetValue("consultantType", ConsultantType, languageCode);
                        //if (model.Language != "" && model.Language != null)
                        // BlogData.SetValue("languages", model.Language, languageCode);
                        if (model.Skills != "" && model.Skills != null)
                            BlogData.SetValue("skillsByAPI", model.Skills, languageCode);
                        // if (model.isCertifier)
                        // BlogData.SetValue("isCertifier", model.isCertifier, languageCode);
                        //if (model.DateOfCertification != "" && model.DateOfCertification != null)
                        //BlogData.SetValue("dateOfCertification", Convert.ToDateTime(dateTime).ToString("dd-MM-yyyy"), languageCode);
                        //if (model.PlaceOfCertification != "" && model.PlaceOfCertification != null)
                        //BlogData.SetValue("placeOfCertification", model.PlaceOfCertification, languageCode);
                        //  if (model.LanguageOfCertification != "" && model.LanguageOfCertification != null)
                        // BlogData.SetValue("languageOfCertification", model.LanguageOfCertification, languageCode);
                        // if (model.WebsiteName != "" && model.WebsiteName != null)
                        //BlogData.SetValue("websiteName", model.WebsiteName, languageCode);
                        ////if (model.InstructorName != "" && model.InstructorName != null)
                        //BlogData.SetValue("instructorName", model.InstructorName, languageCode);
                        //if (conceptsList.Count() > 0 && conceptsList != null)
                        //BlogData.SetValue("conceptCertifiedIn", JsonConvert.SerializeObject(conceptsList), languageCode);
                        contentService.SaveAndPublish(BlogData, languageCode);
                        //umbraco.library.RefreshContent();

                        return BlogData.Id.ToString();
                    }
                    else if (model.ActionType.ToLower() == "delete")
                    {
                        var BlogData = contentService.GetById(model.NodeId);
                        var test = "";
                        var consultantsPageId = Convert.ToInt32(ConfigurationManager.AppSettings["ConsultantsPageId"]);
                        if (BlogData.ContentType.Alias.Equals("partnerCards") && BlogData.ParentId.Equals(consultantsPageId))
                        {
                            contentService.Delete(BlogData);
                            return "Profile delete successfully !!";
                        }
                        else
                        {
                            return "nodeId is not correct.";
                        }
                    }
                    return "";
                }
                else
                {
                    return "Data not found";
                }
            }
            catch (Exception ex)
            {
                return " ActionType--" + model.ActionType
                    + " NodeId--" + model.NodeId
                    + " ConsultantType--" + model.ConsultantType
                    + " ContactNumber--" + model.ContactNumber
                    + " EmployeeSmallImage--" + model.EmployeeSmallImage
                    + " EmployeeName--" + model.EmployeeName
                    + " EmployeeDescription--" + model.EmployeeDescription
                    + "error==" + error + "----" + ex.Message;
            }
        }

        public GuidUdi DownloadImage(string imageUrl, string Name, int mediaParentId)
        {
            //return "";
            httpWebRequest = (HttpWebRequest)WebRequest.Create(imageUrl);
            httpWebRequest.Method = WebRequestMethods.Http.Get;
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
            var orignalPath = "";
            try
            {
                using (HttpWebResponse httpResponse = httpWebRequest.GetResponse() as HttpWebResponse)
                {
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream stream = httpResponse.GetResponseStream();
                        var memoryStream = new MemoryStream();
                        stream.CopyTo(memoryStream);
                        try
                        {
                            // Needed Interfaces
                            //IMediaService _mediaService;

                            // The new way to do it.
                            // IMedia media = _mediaService.CreateMediaWithIdentity(Name, mediaParentId, "File");
                            // media.SetValue(_contentTypeBaseServiceProvider, "umbracoFile", "abc.jpg", memoryStream);


                            Name = Name.Replace(" ", "-");
                            orignalPath = "/img/" + Name;
                            IMediaService mediaService = Services.MediaService;
                            IMedia media = mediaService.CreateMedia(Name, mediaParentId, "Image");
                            //media.SetValue("umbracoFile", memoryStream, "en-Us");
                            // IContentTypeBaseServiceProvider _contentTypeBaseServiceProvider;
                            media.SetValue(_contentTypeBaseServiceProvider, "umbracoFile", Name, memoryStream);
                            mediaService.Save(media);
                            //return string.Concat("umb://media/", media.Key.ToString().Replace("-", ""));
                            return media.GetUdi();
                        }
                        catch (Exception ex)
                        {
                            return null;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }

            return null;
        }

        [HttpPost]
        public int GetEmployee()
        {
            int i = 0;
            //var team = new umbraco.NodeFactory.Node(1161);
            var team = Umbraco.Content(1161);

            // var employeeList = JsonConvert.DeserializeObject<ArchetypeModel>(team.GetProperty("employeeList").Value);
            var employeeList = team.Value<IEnumerable<IPublishedElement>>("employeeList");

            Employee employeeDetails = new Employee();

            List<Employee> employeeDetailsList = new List<Employee>();
            //employeeList.Fieldsets.
            foreach (var employee in employeeList.Where(x => x != null && x.Properties.Any()))
            {
                employeeDetails.EmployeeName = employee.Value<string>("employeeName");
                employeeDetails.Desgination = employee.Value<string>("desgination");
                employeeDetails.ContactNumber = employee.Value<string>("contactNumber");
                employeeDetails.EmployeeSmallImage = GetMediaGuid(employee.Value<int>("employeeSmallImage"));
                employeeDetails.EmployeeDescription = employee.Value<string>("employeeDescription");

                List<SocialMedia> socialMedias = new List<SocialMedia>();
                // var socialmedia = JsonConvert.DeserializeObject<ArchetypeModel>(employee.GetValue("socialMediaList"));
                var socialmedia = employee.Value<IEnumerable<IPublishedElement>>("socialMediaList");
                //socialmedia.Fieldsets
                foreach (var social in socialmedia.Where(x => x != null && x.Properties.Any()))
                {
                    string iconlink = social.Value<string>("iconLink");
                    if (iconlink != null)
                    {
                        if (iconlink.ToLower().StartsWith("mailto:"))
                        {
                            string[] iL = iconlink.Split(new string[] { "mailto:" }, StringSplitOptions.None);
                            iconlink = iL[iL.Length - 1];
                        }
                    }
                    socialMedias.Add(new SocialMedia()
                    {
                        Name = social.Value<string>("name"),
                        IconDescription = social.Value<string>("iconDescription"),
                        IconHeading = social.Value<string>("iconHeading"),
                        IconImage = GetMediaGuid(social.Value<int>("iconImage")),
                        IconLink = social.Value<string>("iconLink")
                    });
                }
                employeeDetails.SocialMediaList = socialMedias;
                employeeDetails.NodeId = int.Parse(employee.Value<string>("employeeType"));

                //employeeDetails.EmployeeType.Add(employeeDetails.NodeId.ToString());

                employeeDetailsList.Add(new Employee()
                {
                    EmployeeName = employeeDetails.EmployeeName,
                    Desgination = employeeDetails.Desgination,
                    EmployeeDescription = employeeDetails.EmployeeDescription,
                    ContactNumber = employeeDetails.ContactNumber,
                    EmployeeSmallImage = employeeDetails.EmployeeSmallImage,
                    NodeId = employeeDetails.NodeId,
                    SocialMediaList = employeeDetails.SocialMediaList
                });

                if (i++ == 0)
                {
                    //var nodeid = UpdateEmployeeProfile(employeeDetails);
                    //CreateEmployeeProfile(employeeDetails);
                }
                //CreateEmployeeProfile(employeeDetails);
            }


            CreateEmployeeProfile(employeeDetailsList);

            //var contentService = Services.ContentService;
            //var employeeList = contentService.GetById(1161).Children();
            //Employee employeeDetails = new Employee();
            //foreach (var item in employeeList)
            //{
            //    if (item.Id != 1407)
            //    {
            //        List<SocialMedia> socialMedias = new List<SocialMedia>();
            //        string obj = item.GetValue("socialMediaList").ToString();
            //        var socialmedia = JsonConvert.DeserializeObject<List<RootObjects>>(obj);

            //        foreach (var it in socialmedia)
            //        {
            //            string iconLink = it.iconLink;
            //            if (it.iconLink != null)
            //            {
            //                if (it.iconLink.Contains("mailto:"))
            //                {
            //                    string[] o = it.iconLink.Split(new string[] { "mailto:" }, StringSplitOptions.None);
            //                    iconLink = o[o.Length - 1];
            //                }
            //            }
            //            socialMedias.Add(new SocialMedia()
            //            {
            //                Name = it.socialMediaName,
            //                IconDescription = it.iconDescription,
            //                IconHeading = it.iconHeading,
            //                IconImage = it.iconImage,
            //                IconLink = iconLink
            //            });
            //        }
            //        employeeDetails.NodeId = item.Id;
            //        employeeDetails.SocialMediaList = socialMedias;
            //        UpdateEmployeeProfile(employeeDetails);
            //    }
            //}

            return 0;
        }

        [HttpPost]
        public int DeleteEmployee()
        {
            var contentService = Services.ContentService;
            var team = contentService.GetById(1161);
            var teamChildren = contentService.GetPagedChildren(1161, 1, 100, out long totalRecords);

            foreach (var item in teamChildren)
            {
                //if (item.Id != 1104)
                //{
                //    contentService.Delete(item);
                //}
                if (item.CreateDate.Date == DateTime.Today)
                {
                    contentService.Delete(item);
                }
            }
            return 0;
        }

        public string CreateEmployeeProfile(List<Employee> model)
        {
            try
            {
                var contentService = Services.ContentService;

                //string EmpName = HttpUtility.UrlDecode(model.EmployeeName, System.Text.Encoding.GetEncoding("iso-8859-1"));

                IContent BlogData = contentService.GetById(1206);


                List<Dictionary<string, object>> keyValues = new List<Dictionary<string, object>>();

                foreach (var itemss in model)
                {
                    List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
                    foreach (var item in itemss.SocialMediaList)
                    {
                        items.Add(new Dictionary<string, object>() {
                            { "socialMediaName" , item.Name},
                            { "iconImage", item.IconImage },
                            { "iconLink", item.IconLink },
                            { "iconHeading", item.IconHeading},
                            { "iconDescription", item.IconDescription}
                        });
                    }

                    List<string> otherCategoriesList = new List<string>();
                    var guid = contentService.GetById(itemss.NodeId).GetUdi().Guid;
                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                    //if (itemss.EmployeeType != null)
                    //{
                    //    foreach (var item in itemss.EmployeeType)
                    //    {
                    //        var guid = contentService.GetById(int.Parse(item)).GetUdi().Guid;
                    //        otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                    //    }
                    //}

                    keyValues.Add(new Dictionary<string, object>() {
                            { "employeeName" , itemss.EmployeeName},
                            { "desgination", itemss.Desgination },
                            { "contactNumber", itemss.ContactNumber },
                            { "employeeSmallImage", itemss.EmployeeSmallImage},
                            { "socialMediaList", JsonConvert.SerializeObject(items)},
                            { "employeeDescription", itemss.EmployeeDescription},
                            { "employeeType", string.Join(",", otherCategoriesList)}
                        });
                }

                BlogData.SetValue("employees", JsonConvert.SerializeObject(keyValues));

                //BlogData.SetValue("employeeName", EmpName);
                //BlogData.SetValue("desgination", model.Desgination);
                //BlogData.SetValue("contactNumber", model.ContactNumber);
                //BlogData.SetValue("employeeSmallImage", model.EmployeeSmallImage);
                //BlogData.SetValue("socialMediaList", JsonConvert.SerializeObject(items));
                //BlogData.SetValue("employeeDescription", model.EmployeeDescription);
                //BlogData.SetValue("employeeType", string.Join(",", otherCategoriesList));
                contentService.SaveAndPublish(BlogData);
                //umbraco.library.RefreshContent();

                return BlogData.Id.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string UpdateEmployeeProfile(Employee model)
        {
            try
            {
                var contentService = Services.ContentService;

                var BlogData = contentService.GetById(model.NodeId);

                List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();

                foreach (var item in model.SocialMediaList)
                {
                    items.Add(new Dictionary<string, object>() {
                        { "socialMediaName" , item.Name},
                        { "iconImage", item.IconImage },
                        { "iconLink", item.IconLink },
                        { "iconHeading", item.IconHeading},
                        { "iconDescription", item.IconDescription}
                    });
                }

                BlogData.SetValue("socialMediaList", JsonConvert.SerializeObject(items));

                contentService.SaveAndPublish(BlogData);
                // umbraco.library.RefreshContent();

                return BlogData.Id.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string GetMediaGuid(int id)
        {
            //var imagesFolder = ApplicationContext.Current.Services.MediaService.GetRootMedia();
            var imagesFolder = Umbraco.MediaAtRoot();
            IMedia image = null;
            Guid guid = Guid.Empty;

            foreach (var item in imagesFolder)
            {
                // if (item == "Folder")
                if (item.ContentType.Alias == "Folder")
                {
                    //var images = ApplicationContext.Current.Services.MediaService.GetDescendants(item.Id);
                    //var images =  Umbraco.MediaAtRoot().FirstOrDefault()?.Descendants().Where(x => x.Id == item.Id).FirstOrDefault()?.Descendants();
                    //foreach (var folderimage in images)
                    //{
                    //    if (folderimage.Id == id)
                    //    {
                    //        image = images.FirstOrDefault(x => x.Id == id);
                    //        guid = image.Key;
                    //        return string.Concat("umb://media/", guid.ToString().Replace("-", ""));
                    //    }
                    //}
                }
                else
                {
                    if (item.Id == id)
                    {
                        //image = imagesFolder.FirstOrDefault(x => x.Id == id);
                        //guid = image.Key;
                        //return string.Concat("umb://media/", guid.ToString().Replace("-", ""));
                    }
                }
            }

            return "";
        }

        /// <summary>
        /// Adding nodes under parters node
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public string AddPartnerHub(PartnerHubModel model)
        {
            try
            {
                if (model != null)
                {
                    var languageCode = "en-US";
                    if (!string.IsNullOrEmpty(model.LanguageCode) && (model.LanguageCode.Contains("en-Us") || model.LanguageCode.Contains("de-DE")))
                    {
                        languageCode = model.LanguageCode;
                    }

                    //var contentService = Services.ContentService;
                    var contentService = Current.Services.ContentService;
                    if (model.ActionType.ToLower() == "create" || model.ActionType.ToLower() == "update")
                    {
                        IContent PartnerHub = null;
                        if (model.ActionType.ToLower() == "create")
                        {
                            var apiNodeId = Convert.ToInt32(ConfigurationManager.AppSettings["PartnersPageId"]);
                            PartnerHub = contentService.Create(model.Name, apiNodeId, "partnerCards");
                            PartnerHub.Key = new Guid();
                        }
                        else
                        {
                            if (model.NodeId < 1)
                            {
                                return "Please provide node id !!";
                            }
                            PartnerHub = contentService.GetById(model.NodeId);
                        }
                        PartnerHub.SetCultureName(model.Name, languageCode);
                        if (!string.IsNullOrEmpty(model.Logo))
                        {
                            var mediaNodeId = Convert.ToInt32(ConfigurationManager.AppSettings["MediaNodeId"]);
                            string[] image = model.Logo.Split('/');
                            if (image != null && image.Length > 0)
                            {
                                GuidUdi guidUdi = DownloadImage(model.Logo, image[image.Length - 1], mediaNodeId);
                                if (guidUdi != null)
                                {
                                    PartnerHub.SetValue("partnersImage", guidUdi.ToString(), languageCode);
                                }
                            }
                        }

                        PartnerHub.SetValue("partnerDescription", model.Description != null ? model.Description : string.Empty, languageCode);
                        PartnerHub.SetValue("partnerType", "Company", languageCode);
                        contentService.SaveAndPublish(PartnerHub);
                        return PartnerHub.Id.ToString();
                    }
                    else if (model.ActionType.ToLower() == "delete")
                    {
                        if (model.NodeId < 1)
                        {
                            return "Please provide node id !!";
                        }
                        var PartnerHub = contentService.GetById(model.NodeId);
                        contentService.Delete(PartnerHub);
                        return "Partner Hub delete successfully !!";
                    }
                    return "Please provide action type !!";
                }

                return "Please provide data for partner hub !!";

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public PartialViewResult Consultancies()
        {
            return PartialView();
        }
        public PartialViewResult Partnerships()
        {
            return PartialView();
        }
        public PartialViewResult theActeejourney()
        {
            return PartialView();
        }
    }

    //[RuntimeLevel(MinLevel = RuntimeLevel.Run)]
    //public class RegisterServicesComposer : IUserComposer
    //{
    //    public void Compose(Composition composition)
    //    {
    //        composition.Register(typeof(IContentTypeBaseServiceProvider),
    //            typeof(ContentTypeBaseServiceProvider), Lifetime.Request);
    //    }
    //}
}