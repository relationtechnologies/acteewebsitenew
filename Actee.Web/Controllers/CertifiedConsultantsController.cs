﻿using Actee.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Http.Cors;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.WebApi;
namespace Actee.Web.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CertifiedConsultantsController : UmbracoApiController
    {
        private readonly IContentTypeBaseServiceProvider _contentTypeBaseServiceProvider;
        private HttpWebRequest httpWebRequest;

        /// <summary>
        /// This Method is for Image
        /// </summary>
        /// <param name="imageUrl"></param>
        /// <param name="Name"></param>
        /// <param name="mediaParentId"></param>
        /// <returns></returns>
        private GuidUdi DownloadImage(string imageUrl, string Name, int mediaParentId)
        {
            httpWebRequest = (HttpWebRequest)WebRequest.Create(imageUrl);
            httpWebRequest.Method = WebRequestMethods.Http.Get;
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
            var orignalPath = "";
            try
            {
                using (HttpWebResponse httpResponse = httpWebRequest.GetResponse() as HttpWebResponse)
                {
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream stream = httpResponse.GetResponseStream();
                        var memoryStream = new MemoryStream();
                        stream.CopyTo(memoryStream);
                        try
                        {
                            Name = Name.Replace(" ", "-");
                            orignalPath = "/img/" + Name;
                            IMediaService mediaService = Services.MediaService;
                            IMedia media = mediaService.CreateMedia(Name, mediaParentId, "Image");
                            media.SetValue(_contentTypeBaseServiceProvider, "umbracoFile", Name, memoryStream);
                            mediaService.Save(media);
                            return media.GetUdi();
                        }
                        catch (Exception ex)
                        {
                            return null;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }

            return null;
        }


        /// <summary>
        /// This Method is for create CertifiedConsultant
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public string CertifiedConsultant(Employee model)
        {
            string error = "0";
            try
            {
                if (model != null)
                {
                    var languageName = string.Empty;

                    if (!string.IsNullOrEmpty(model.Language))
                        languageName = new CultureInfo(model.Language).DisplayName;

                    error = "1";
                    var contentService = Services.ContentService;
                    error = "1.1";
                    if (model.ActionType.ToLower() == "create" || model.ActionType.ToLower() == "update")
                    {
                        error = "2";
                        IContent BlogData = null;
                        error = "2.1";
                        if (model.ActionType.ToLower() == "create")
                        {
                            var consultantsPageId = Convert.ToInt32(ConfigurationManager.AppSettings["ConsultantsPageId"]);
                            BlogData = contentService.Create(model.EmployeeName, consultantsPageId, "partnerCards");
                            BlogData.Key = new Guid();
                        }
                        else
                        {
                            BlogData = contentService.GetById(model.NodeId);
                            BlogData.Name = (model.EmployeeName != "" && model.EmployeeName != null) ? model.EmployeeName : BlogData.Name;
                        }
                        BlogData.SetCultureName(model.EmployeeName, "en-US");
                        BlogData.SetCultureName(model.EmployeeName, "de-DE");
                        List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
                        error = "3";
                        if (model.SocialMediaList != null && model.SocialMediaList.Count() > 0)
                        {
                            foreach (var item in model.SocialMediaList)
                            {
                                string mediaid = "";
                                var guidimage = "7b6ab2b5-735e-49cc-b77f-ecc0c2f63fac";

                                if (item.Name.ToLower().Contains("link"))
                                {
                                    guidimage = "7b6ab2b5-735e-49cc-b77f-ecc0c2f63fac";
                                }
                                else if (item.Name.ToLower().Contains("mail"))
                                {
                                    guidimage = "e25bc393-1770-4c90-8321-adb28020ec8c";
                                }
                                mediaid = string.Concat("umb://media/", guidimage.Replace("-", ""));
                                items.Add(new Dictionary<string, object>() {
                                    { "key", new Guid() },
                                    { "name", item.Name },
                                    { "ncContentTypeAlias", "socialMediaNC"},
                                    { "socialMediaName" , item.Name},
                                    { "iconImage", mediaid },
                                    { "iconLink", item.IconLink },
                                    { "iconHeading", item.IconHeading},
                                    { "iconDescription", item.IconDescription}
                                });
                            }
                        }
                        List<string> otherCategoriesList = new List<string>();
                        error = "4";
                        if (model.EmployeeType != null)
                        {
                            foreach (var item in model.EmployeeType)
                            {
                                long count;
                                var blogcat = contentService.GetPagedChildren(1407, 0, 1000, out count);
                                if (blogcat.Count() <= 0)
                                {
                                    blogcat = contentService.GetPagedChildren(3285, 0, 1000, out count);
                                }
                                foreach (var cat in blogcat)
                                {
                                    var guid = contentService.GetById(cat.Id).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }
                            }
                        }
                        int countryId = 0;
                        error = "5";
                        if (model.CountriesOfOperation != null)
                        {
                            foreach (var item in model.CountriesOfOperation)
                            {
                                long count2;
                                var LocationsId = Convert.ToInt32(ConfigurationManager.AppSettings["LocationsId"]);
                                var countries = contentService.GetPagedChildren(LocationsId, 0, 1000, out count2);
                                int isCountryExist = 0;
                                int catId = 0;
                                if (countries.Count() > 0)
                                {
                                    foreach (var cat in countries)
                                    {
                                        if (cat.Name == item.name)
                                        {
                                            catId = cat.Id;
                                            isCountryExist = 1;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    Country country = new Country();
                                    country.name = item.name ?? string.Empty;
                                    country.code = item.code ?? string.Empty;
                                    var guid = contentService.GetById(int.Parse(AddNewCountry(country))).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }

                                if (isCountryExist == 1)
                                {
                                    var guid = contentService.GetById(catId).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }
                                else
                                {
                                    Country country = new Country();
                                    country.name = item.name;
                                    country.code = item.code;

                                    countryId = int.Parse(AddNewCountry(country));

                                    var guid = contentService.GetById(countryId).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }
                            }
                        }
                        error = "6";
                        List<Dictionary<string, object>> conceptsList = new List<Dictionary<string, object>>();
                        if (model.ConceptCertifiedIn != null)
                        {
                            foreach (var conce in model.ConceptCertifiedIn)
                            {
                                if (conce.IsApproved)
                                {
                                    conceptsList.Add(new Dictionary<string, object>() {
                                        { "key", new Guid() },
                                        { "name", conce.name },
                                        { "ncContentTypeAlias", "conceptCertifiedNC"},
                                        { "conceptName", conce.name},
                                        { "value", conce.value},
                                        { "instructorName", conce.instructorName},
                                        { "certificationDate", conce.certificationDate},
                                        { "placeOfCertification", conce.placeOfCertification},
                                        { "approvedDate", conce.approvedDate}
                                    });
                                }
                            }
                        }

                        DateTime dateTime = new DateTime();

                        if (model.DateOfCertification != null)
                        {
                            dateTime = DateTime.Parse(model.DateOfCertification);
                        }

                        string[] image = model.EmployeeSmallImage != "" && model.EmployeeSmallImage != null ? model.EmployeeSmallImage.Split('/') : null;

                        var languageCodeEn = "en-US";
                        var languageCodeDe = "de-DE";
                        if (model.EmployeeName != "" && model.EmployeeName != null)
                        {
                            BlogData.Name = model.EmployeeName;
                            BlogData.SetValue("partnerName", model.EmployeeName, languageCodeEn);
                            BlogData.SetValue("partnerName", model.EmployeeName, languageCodeDe);
                        }
                        if (model.Desgination != "" && model.Desgination != null)
                            BlogData.SetValue("partnerAddress", model.Desgination, languageCodeEn);
                        BlogData.SetValue("partnerAddress", model.Desgination, languageCodeDe);
                        if (model.EmployeeSmallImage != "" && model.EmployeeSmallImage != null)
                        {
                            var mediaNodeId = Convert.ToInt32(ConfigurationManager.AppSettings["MediaNodeId"]);
                            GuidUdi guidUdi = DownloadImage(model.EmployeeSmallImage, image[image.Length - 1], mediaNodeId);
                            if (guidUdi != null)
                            {
                                BlogData.SetValue("partnersImage", guidUdi.ToString(), languageCodeEn);
                                BlogData.SetValue("partnersImage", guidUdi.ToString(), languageCodeDe);
                            }

                        }
                        BlogData.SetValue("partnerAddressLink", model.WebsiteName != null ? model.WebsiteName : string.Empty, languageCodeEn);
                        BlogData.SetValue("partnerAddressLink", model.WebsiteName != null ? model.WebsiteName : string.Empty, languageCodeDe);
                        if (model.SocialMediaList.Count() > 0)
                        {
                            BlogData.SetValue("partnerEmail", model.SocialMediaList[0].IconLink != null ? model.SocialMediaList[0].IconLink : string.Empty, languageCodeEn);
                            BlogData.SetValue("partnerEmail", model.SocialMediaList[0].IconLink != null ? model.SocialMediaList[0].IconLink : string.Empty, languageCodeDe);
                        }
                        BlogData.SetValue("contactNumber", model.ContactNumber != null ? model.ContactNumber : string.Empty, languageCodeEn);
                        BlogData.SetValue("contactNumber", model.ContactNumber != null ? model.ContactNumber : string.Empty, languageCodeDe);
                        BlogData.SetValue("partnerLanguages", languageName != null ? languageName : string.Empty, languageCodeEn);
                        BlogData.SetValue("partnerLanguages", languageName != null ? languageName : string.Empty, languageCodeDe);
                        if (model.EmployeeDescription != "" && model.EmployeeDescription != null)
                            BlogData.SetValue("partnerDescription", model.EmployeeDescription != null ? model.EmployeeDescription : string.Empty, languageCodeEn);
                        BlogData.SetValue("partnerDescription", model.EmployeeDescription != null ? model.EmployeeDescription : string.Empty, languageCodeDe);
                        BlogData.SetValue("partnerType", "Consultant", languageCodeEn);
                        BlogData.SetValue("partnerType", "Consultant", languageCodeDe);
                        if (model.Skills != "" && model.Skills != null)
                            BlogData.SetValue("skillsByAPI", model.Skills, languageCodeEn);
                        BlogData.SetValue("skillsByAPI", model.Skills, languageCodeDe);
                        contentService.SaveAndPublish(BlogData, languageCodeEn);
                        contentService.SaveAndPublish(BlogData, languageCodeDe);
                        return BlogData.Id.ToString();
                    }
                    else if (model.ActionType.ToLower() == "delete")
                    {
                        var BlogData = contentService.GetById(model.NodeId);
                        var consultantsPageId = Convert.ToInt32(ConfigurationManager.AppSettings["ConsultantsPageId"]);
                        if (BlogData.ContentType.Alias.Equals("partnerCards") && BlogData.ParentId.Equals(consultantsPageId))
                        {
                            contentService.Delete(BlogData);
                            return "Profile delete successfully !!";
                        }
                        else
                        {
                            return "nodeId is not correct.";
                        }
                    }
                    return "";
                }
                else
                {
                    return "Data not found";
                }
            }
            catch (Exception ex)
            {
                Logger.Info<CertifiedConsultantsController>("CertifiedConsultants When adding from API: " + ex.Message);
                return " ActionType--" + model.ActionType
                    + " NodeId--" + model.NodeId
                    + " ConsultantType--" + model.ConsultantType
                    + " ContactNumber--" + model.ContactNumber
                    + " EmployeeSmallImage--" + model.EmployeeSmallImage
                    + " EmployeeName--" + model.EmployeeName
                    + " EmployeeDescription--" + model.EmployeeDescription
                    + "error==" + error + "----" + ex.Message + "-----" + ex.StackTrace + "---" + (ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }

        /// <summary>
        /// This method is for add country
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        private string AddNewCountry(Country country)
        {
            try
            {
                var contentService = Services.ContentService;
                long children;
                var LocationsId = Convert.ToInt32(ConfigurationManager.AppSettings["LocationsId"]);
                var existNode = contentService.GetPagedChildren(LocationsId, 0, 1000, out children).Where(x => x.GetValue<string>("locationName", "en-US").Equals(country.name)).FirstOrDefault();
                if (existNode == null)
                {
                    var BlogData = contentService.Create(country.name, LocationsId, "location");
                    if (country.name != "" && country.name != null)
                        BlogData.SetValue("locationName", country.name, "en-US");
                    BlogData.SetCultureName(country.name, "en-US");
                    contentService.SaveAndPublish(BlogData, "en-US");
                    return BlogData.Id.ToString();
                }
                else
                {
                    return existNode.Id.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}