﻿using Actee.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace Actee.Web.Controllers
{
    public class ContactFormController : SurfaceController
    {
        [HttpPost]
        public ActionResult ContactUsAjax(ContactModel model)
        {
            try
            {
                string name = model.Name;
                string email = model.Email;
                string subject = model.Subject;
                string message = model.Message;

                if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(subject))
                {
                    string contactPersonEmail = WebConfigurationManager.AppSettings["contactPersonEmail"].ToString();


                    MailSender mail = new MailSender();
                    string html = "<div>";
                    html += "<b style='margin-right:10px;'>Name:</b>" + name + "<br><br>";
                    html += "<b style='margin-right:10px;'>User email:</b>" + email + "<br><br>";
                    html += "<b style='margin-right:10px;'>Message:</b>" + message + "<br>";
                    html += "</div>";

                    string finalHtml = mail.getMailHTML(model.Subject + " Contact", "Admin", html);

                    if (mail.sendMail(contactPersonEmail, finalHtml, "Actee Contact"))
                    {
                        return Json(new ContactFormResult
                        {
                            result = true,
                            message = "Message Sent Successfully."
                        }, JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return Json(new ContactFormResult
                        {
                            result = false,
                            message = "Some unexpected error !"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new ContactFormResult
                    {
                        result = false,
                        message = "Please fill Details !"
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ee)
            {
                //return RedirectToCurrentUmbracoPage();
                return Json(new ContactFormResult
                {
                    result = false,
                    message = "Some unexpected error !"
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult NewsletterSubscribe(string email)
        {

            try
            {
                //string subject = "Newsletter Subscription";
                string message = "Hello, I wants to subscribe on Actee Newsletter with Email: " + email;


                string contactPersonEmail = WebConfigurationManager.AppSettings["contactPersonEmail"].ToString();


                MailSender mail = new MailSender();
                string html = "<div>";

                html += "<b style='margin-right:10px;'>User email:</b>" + email + "<br><br>";
                html += "<b style='margin-right:10px;'>Message:</b>" + message + "<br>";
                html += "</div>";

                //string finalHtml = mail.getMailHTML(subject, "Admin", html);

                if (mail.sendMail("sspl.user42@gmail.com", html, "Newsletter Subscription"))
                {
                    return Json(new ContactFormResult
                    {
                        result = true,
                        message = "Message Sent Successfully."
                    }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new ContactFormResult
                    {
                        result = false,
                        message = "some unexpected error !"
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ee)
            {

                return Json(new ContactFormResult
                {
                    result = false,
                    message = "Some unexpected error !"
                }, JsonRequestBehavior.AllowGet);
            }


        }
    }
    public class ContactFormResult
    {
        public bool result { get; set; }
        public string message { get; set; }
    }
}
