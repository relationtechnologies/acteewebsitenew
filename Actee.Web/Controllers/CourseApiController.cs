﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;
using static Actee.Web.Models.ApiModels;

namespace Actee.Web.Controllers
{
    public class CourseApiController : UmbracoApiController
    {
        [HttpGet]
        public Result GetAllCourse(string Email)
        {
            Result Response = new Result();
            List<CourseList> CourseList = new List<CourseList>();
            try
            {
                var courses = Umbraco.AssignedContentItem.AncestorsOrSelf(1).FirstOrDefault()?.Children(x => x.Id == 1144 && x.IsVisible()).FirstOrDefault()?.Children;
                if (!string.IsNullOrEmpty(Email))
                    courses = courses.Where(a => a.Value<string>("userEmail") == Email);
                foreach (var item in courses)
                {
                    int TotalSignupUser = 0;
                    if (item.Children != null && item.Children.FirstOrDefault() != null && item.Children.FirstOrDefault().Children != null)
                    {
                        TotalSignupUser = item.Children.FirstOrDefault().Children.Count();
                    }
                    CourseList.Add(new CourseList
                    {
                        Id = item.Key,
                        CourseName = item.Value<string>("courseName"),
                        StartDate = item.Value<string>("startDate"),
                        Duration = item.Value<string>("duration"),
                        LanguageType = item.Value<string>("languageType"),
                        TotalSignupUser = TotalSignupUser
                    });
                }
                Response.data = CourseList;
                Response.success = true;
                Response.Message = "Courses List Found.";
            }
            catch (Exception ex)
            {
                Response.data = null;
                Response.success = false;
                Response.Message = ex.Message;
            }
            return Response;
        }

        [HttpGet]
        public Result GetCourseDetailById(Guid id)
        {
            Result Response = new Result();
            Course CourseDetails = new Course();
            try
            {
                var courses = Umbraco.Content(id);
                CourseDetails.Id = courses.Key;
                CourseDetails.NodeName = courses.Name;
                CourseDetails.MetaTitle = courses.Value<string>("pageTitle");
                CourseDetails.MetaKeywords = courses.Value<string>("metaKeywords");
                CourseDetails.MetaDescription = courses.Value<string>("metaDescription");
                CourseDetails.BannerHeading = courses.Value<string>("heading");
                CourseDetails.BannerDescription = courses.Value<string>("description");
                CourseDetails.BannerImage = courses.Value<string>("image");
                CourseDetails.BannerImagePath = getImage(Convert.ToInt32(CourseDetails.BannerImage));
                CourseDetails.Title = courses.Value<string>("title");
                CourseDetails.CourseDescription = courses.Value<string>("courseDescription");
                CourseDetails.CoursePracticalInformation = courses.Value<string>("coursePracticalInformation");
                CourseDetails.CoursePrice = courses.Value<string>("coursePrice");
                CourseDetails.CourseName = courses.Value<string>("courseName");
                CourseDetails.StartDate = courses.Value<string>("startDate");
                CourseDetails.Duration = courses.Value<string>("duration");
                CourseDetails.LanguageType = courses.Value<string>("languageType");
                CourseDetails.Location = courses.Value<string>("location");
                CourseDetails.Price = courses.Value<string>("price");
                CourseDetails.UserName = courses.Value<string>("userName");
                CourseDetails.UserEmail = courses.Value<string>("userEmail");
                CourseDetails.IconImage = courses.Value<string>("iconImage");
                CourseDetails.IconImagePath = getImage(Convert.ToInt32(CourseDetails.IconImage));
                if (courses.Children != null && courses.Children.FirstOrDefault() != null && courses.Children.FirstOrDefault().Children != null)
                {
                    CourseDetails.TotalSignupUser = courses.Children.FirstOrDefault().Children.Count();
                }
                Response.data = CourseDetails;
                Response.success = true;
                Response.Message = "Course Details Found.";
            }
            catch (Exception ex)
            {
                Response.data = null;
                Response.success = false;
                Response.Message = ex.Message;
            }
            return Response;
        }

        [HttpPost]
        public Result UpdateCourse(Course course)
        {
            Result Response = new Result();
            var contentService = Services.ContentService;
            try
            {
                var CourseDetails = contentService.GetById(course.Id);
                CourseDetails.Name = course.NodeName;
                CourseDetails.SetValue("pageTitle", course.MetaTitle);
                CourseDetails.SetValue("metaKeywords", course.MetaKeywords);
                CourseDetails.SetValue("metaDescription", course.MetaDescription);
                CourseDetails.SetValue("heading", course.BannerHeading);
                CourseDetails.SetValue("description", course.BannerDescription);
                CourseDetails.SetValue("image", course.BannerImage);
                CourseDetails.SetValue("iconImage", course.IconImage);
                CourseDetails.SetValue("title", course.Title);
                CourseDetails.SetValue("courseDescription", course.CourseDescription);
                CourseDetails.SetValue("coursePracticalInformation", course.CoursePracticalInformation);
                CourseDetails.SetValue("coursePrice", course.CoursePrice);
                CourseDetails.SetValue("productPlanId", course.Id);
                CourseDetails.SetValue("courseName", course.CourseName);
                CourseDetails.SetValue("startDate", course.StartDate);
                CourseDetails.SetValue("duration", course.Duration);
                CourseDetails.SetValue("languageType", course.LanguageType);
                CourseDetails.SetValue("location", course.Location);
                CourseDetails.SetValue("price", course.Price);
                CourseDetails.SetValue("userName", course.UserName);
                CourseDetails.SetValue("userEmail", course.UserEmail);
                contentService.SaveAndPublish(CourseDetails);
                Response.data = true;
                Response.success = true;
                Response.Message = "Course Updated Successfully.";
            }
            catch (Exception ex)
            {
                Response.data = null;
                Response.success = false;
                Response.Message = ex.Message;
            }
            return Response;
        }

        [HttpGet]
        public Result GetAllSignUpUserByCourseId(Guid id)
        {
            Result Response = new Result();
            List<User> Users = new List<User>();
            var courses = Umbraco.Content(id);
            try
            {
                var userlist = Umbraco.Content(id).Children.FirstOrDefault()?.Children;
                foreach (var item in userlist)
                {
                    Users.Add(new User
                    {
                        Id = item.Key,
                        UserName = item.Value<string>("courseUserName"),
                        Course = item.Value<string>("course"),
                        Email = item.Value<string>("email"),
                        Message = item.Value<string>("messages")
                    });
                }
                Response.data = Users;
                Response.success = true;
                Response.Message = "Users Found.";
            }
            catch (Exception ex)
            {
                Response.data = null;
                Response.success = false;
                Response.Message = ex.Message;
            }
            return Response;
        }

        [HttpPost]
        public Result AddCourse(Course course)
        {
            Result Response = new Result();
            var contentService = Services.ContentService;
            var CourseDetails = contentService.Create(course.NodeName, 1144, "courseSinglePage");
            try
            {
                CourseDetails.SetValue("pageTitle", course.MetaTitle);
                CourseDetails.SetValue("metaKeywords", course.MetaKeywords);
                CourseDetails.SetValue("metaDescription", course.MetaDescription);
                CourseDetails.SetValue("heading", course.BannerHeading);
                CourseDetails.SetValue("description", course.BannerDescription);
                CourseDetails.SetValue("image", course.BannerImage);
                CourseDetails.SetValue("iconImage", course.IconImage);
                CourseDetails.SetValue("title", course.Title);
                CourseDetails.SetValue("courseDescription", course.CourseDescription);
                CourseDetails.SetValue("coursePracticalInformation", course.CoursePracticalInformation);
                CourseDetails.SetValue("coursePrice", course.CoursePrice);
                CourseDetails.SetValue("productPlanId", course.Id);
                CourseDetails.SetValue("courseName", course.CourseName);
                CourseDetails.SetValue("startDate", course.StartDate);
                CourseDetails.SetValue("duration", course.Duration);
                CourseDetails.SetValue("languageType", course.LanguageType);
                CourseDetails.SetValue("location", course.Location);
                CourseDetails.SetValue("price", course.Price);
                CourseDetails.SetValue("userName", course.UserName);
                CourseDetails.SetValue("userEmail", course.UserEmail);
                contentService.SaveAndPublish(CourseDetails);

                var blankFolder = contentService.Create("User", CourseDetails.Id, "blankFolder");
                contentService.SaveAndPublish(blankFolder);

                Response.data = true;
                Response.success = true;
                Response.Message = "Course Added Successfully.";
            }
            catch (Exception ex)
            {
                Response.data = null;
                Response.success = false;
                Response.Message = ex.Message;
            }
            return Response;
        }

        [HttpGet]
        public Result GetUserDetailsById(Guid id)
        {
            Result Response = new Result();
            User Users = new User();
            var contentService = Services.ContentService;
            try
            {
                var users = contentService.GetById(id);
                Users.Id = users.Key;
                Users.UserName = users.GetValue<string>("userName");
                Users.Course = users.GetValue<string>("course");
                Users.Email = users.GetValue<string>("email");
                Users.Message = users.GetValue<string>("message");
                Response.data = Users;
                Response.success = true;
                Response.Message = "User Details Found.";
            }
            catch (Exception ex)
            {
                Response.data = null;
                Response.success = false;
                Response.Message = ex.Message;
            }
            return Response;
        }

        [HttpPost]
        public Result DeleteCourse(Guid id)
        {
            Result Response = new Result();
            User Users = new User();
            var contentService = Services.ContentService;
            try
            {
                var courses = contentService.GetById(id);
                contentService.Delete(courses);
                Response.data = true;
                Response.success = true;
                Response.Message = "Course Deleted Successfully.";
            }
            catch (Exception ex)
            {
                Response.data = null;
                Response.success = false;
                Response.Message = ex.Message;
            }
            return Response;
        }
        [HttpPost]
        public Result UploadMediaFile()
        {
            Result Response = new Result();
            try
            {
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var file = HttpContext.Current.Request.Files["file"];

                    if (file != null)
                    {
                        if (IsImage(file))
                        {
                            var mediaService = Services.MediaService;
                            var media = mediaService.CreateMedia(file.FileName, 1563, "Image");
                            mediaService.Save(media);

                            media.SetValue("umbracoFile", file);
                            mediaService.Save(media);
                            Response.data = media.Id;
                            Response.success = true;
                            Response.Message = "Media Uploaded Successfully.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.data = null;
                Response.success = false;
                Response.Message = ex.Message;
            }
            return Response;
        }
        private bool IsImage(HttpPostedFile file)
        {
            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }
        private IContent SetValue(string p1, string p2)
        {
            throw new NotImplementedException();
        }
        public string getImage(int ImageID)
        {
            var xpNodeIter = Umbraco.Media(ImageID);
            return xpNodeIter.Url();
        }
    }

}