﻿using Actee.Web.Helpers;
using Actee.Web.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Umbraco.Web.Mvc;

namespace Actee.Web.Controllers
{
    public class CourseController : SurfaceController
    {
        [HttpPost]
        public ActionResult GetCourses(int id, int skipRecords, int noOfRecords)
        {
            Courses model = new Courses();
            model.course = Extensions.GetCourses(Umbraco.Content(id), skipRecords, noOfRecords);
            return PartialView("~/Views/Partials/CourseLoadMore.cshtml", model);
        }
        [HttpPost]
        public ActionResult GetClient(int id, int skipRecords, int noOfRecords)
        {
            Client model = new Client();
            model.clients = Extensions.GetClients(Umbraco.Content(id), skipRecords, noOfRecords);
            return PartialView("~/Views/Partials/ClientLoadMore.cshtml", model);
        }
        [HttpPost]
        public ActionResult CourseSignup(CourseSignupModel signup)
        {
            try
            {

                var contentService = Services.ContentService;
                int userparentid = 0;
                var decodeid = Extensions.Decode(signup.modelID);
                var modelid = int.Parse(decodeid);
                if (!contentService.HasChildren(modelid))
                {
                    var user = contentService.Create(
                  "User", // the name of the product
                   modelid, // the parent id should be the id of the group node 
                   "blankFolder", // the alias of the product Document Type
                   0);
                    contentService.SaveAndPublish(user);
                    userparentid = user.Id;
                }
                else
                {
                    //userparentid = contentService.GetById(modelid).Children.FirstOrDefault().Id;
                    userparentid = contentService.GetPagedChildren(modelid, 0, 100, out long totalRecords).FirstOrDefault().Id;

                }
                var app = contentService.Create(
                signup.Name, // the name of the product
                 userparentid, // the parent id should be the id of the group node 
                 "courseSignUp", // the alias of the product Document Type
                 0);

                app.SetValue("courseUserName", signup.Name);
                app.SetValue("email", signup.Email);
                app.SetValue("course", signup.course);
                app.SetValue("messages", signup.Message);
                contentService.SaveAndPublish(app);
                if (ModelState.IsValid)
                {
                    try
                    {
                        MailMessage mail = new MailMessage();
                        var receiver = ConfigurationManager.AppSettings["MailReceiver"].ToString();
                        mail.To.Add(receiver);
                        mail.Subject = "Signup Mail";
                        var username = ConfigurationManager.AppSettings["SenderAddress"].ToString();
                        mail.From = new MailAddress(username);
                        string Body = "Name=" + signup.Name + " ," + "Email=" + signup.Email + " ," + "Course=" + signup.course + " ," + "Message=" + signup.Message;
                        mail.Body = Body;
                        mail.IsBodyHtml = true;
                        using (var smtp = new SmtpClient())
                        {
                            var credential = new NetworkCredential
                            {
                                UserName = ConfigurationManager.AppSettings["MailSenderUserName"].ToString(),
                                Password = ConfigurationManager.AppSettings["MailSenderPass"].ToString()

                            };
                            smtp.Credentials = credential;
                            smtp.Host = ConfigurationManager.AppSettings["MailHost"].ToString();
                            smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["PortNumber"].ToString());
                            smtp.EnableSsl = true;
                            smtp.Send(mail);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception)
            {
                ViewBag.Error = "Some Error";
            }
            return RedirectToCurrentUmbracoUrl();


        }
    }
}