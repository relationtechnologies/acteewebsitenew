﻿using Actee.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Mvc;
using static Actee.Web.Models.ApiModels;

namespace Actee.Web.Controllers
{
    public class GamesController : SurfaceController
    {
        public GamesController()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// This Method is for Search Game
        /// </summary>
        /// <param name="id"></param>
        /// <param name="searchVal"></param>
        /// <returns></returns>
        public JsonResult SearchGames(string id, string searchVal)
        {
            try
            {
                var gamesId = Convert.ToInt32(id);
                var categories = Umbraco.Content(gamesId).Children;
                List<IPublishedContent> games = new List<IPublishedContent>();
                foreach (var item in categories)
                {
                    games.AddRange(item.Children.ToList());
                }

                if (games.Count() > 0)
                {
                    var test = games.FirstOrDefault().GetProperty("gameHeading").GetValue();
                    games = games.Where(x => Convert.ToString(x.GetProperty("gameHeading").GetValue()).ToLower().Contains(searchVal.Trim().ToLower()) || x.Parent.Name.ToLower().Contains(searchVal.Trim().ToLower())).ToList();
                }

                var str = RenderPartialViewToString("_searchGames", games);
                return Json(str, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Info<GamesController>("Error Message: " + e.Message + "\n Error Inner Exp: " + e.InnerException);
                return Json(null, JsonRequestBehavior.AllowGet);
            }

        }


        /// <summary>
        /// This Method is for Search News
        /// </summary>
        /// <param name="id"></param>
        /// <param name="searchVal"></param>
        /// <returns></returns>
        public JsonResult SearchNews(string id, string searchVal)
        {
            try
            {
                var newsId = Convert.ToInt32(id);
                var newsList = Umbraco.Content(newsId).Children.Where(x=>x.ContentType.Alias.Equals("singleNews"));
                newsList = newsList.Where(x => Convert.ToString(x.GetProperty("newsHeading").GetValue()).ToLower().Contains(searchVal.Trim().ToLower()) || Convert.ToString(x.GetProperty("newsType").GetValue()).ToLower().Contains(searchVal.Trim().ToLower())).ToList();

                var str = RenderPartialViewToString("_searchNews", newsList);
                return Json(str, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Info<GamesController>("Error Message: " + e.Message + "\n Error Inner Exp: " + e.InnerException);
                return Json(null, JsonRequestBehavior.AllowGet);
            }

        }
        /// <summary>
        /// This Method is for search Partners
        /// </summary>
        /// <param name="id"></param>
        /// <param name="searchVal"></param>
        /// <returns></returns>
        public JsonResult SearchPartners(string id, string searchVal)
        {
            try
            {
                var partnerId = Convert.ToInt32(id);
                var allPartnerIds = Umbraco.Content(partnerId).Parent.Children.Select(x=>x.Id);
                List<IPublishedContent> partnerList = new List<IPublishedContent>();
                foreach(var pid in allPartnerIds)
                {
                    partnerList.AddRange(Umbraco.Content(pid).Children);
                }
                partnerList = partnerList.Where(x => Convert.ToString(x.GetProperty("partnerAddress").GetValue()).ToLower().Contains(searchVal.Trim().ToLower()) || Convert.ToString(x.GetProperty("partnerName").GetValue()).ToLower().Contains(searchVal.Trim().ToLower())).ToList();

                var str = RenderPartialViewToString("_searchPartners", partnerList);
                return Json(str, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Info<GamesController>("Error Message: " + e.Message + "\n Error Inner Exp: " + e.InnerException);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// This Method is for Search Contacts
        /// </summary>
        /// <param name="id"></param>
        /// <param name="searchVal"></param>
        /// <returns></returns>

        public JsonResult SearchContacts(string id, string searchVal)
        {
            try
            {
                var contactId = Convert.ToInt32(id);
                var contactList = Umbraco.Content(contactId).Children;

                List<IPublishedContent> contacts = new List<IPublishedContent>();
                var ids = contactList.Select(x => x.Id).ToList();

                foreach(var pageId in ids)
                {
                    contacts.AddRange(Umbraco.Content(pageId).Children.ToList());
                }
                if (contacts.Count() > 0)
                {
                    var test = contacts.FirstOrDefault().GetProperty("partnerAddress").GetValue();
                    contacts = contacts.Where(x => Convert.ToString(x.GetProperty("partnerAddress").GetValue()).ToLower().Contains(searchVal.Trim().ToLower()) || Convert.ToString(x.GetProperty("partnerName").GetValue()).ToLower().Contains(searchVal.Trim().ToLower())).ToList();
                }
                var str = RenderPartialViewToString("_searchContacts", contacts);
                return Json(str, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Info<GamesController>("Error Message: " + e.Message + "\n Error Inner Exp: " + e.InnerException);
                return Json(null, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// Get Property Name from Object
        /// </summary>
        /// <param name="propertyName"></param>
        private string GetPropertyName(IPublishedElement obj, string cpAlias, string strPropertyName)
        {
            var propertyName = obj.GetProperty(cpAlias).GetValue();
            var name = propertyName.GetType().GetProperty(strPropertyName);
            var value = name.GetValue(propertyName, null);
            return value?.ToString().ToLower() ?? string.Empty;
        }

        
        public JsonResult SearchFAQ(string id, string searchVal)
        {
            try
            {
                var faqId = Convert.ToInt32(id);
                var faqContent = Umbraco.Content(faqId);
                var faqList = (IEnumerable<IPublishedElement>)faqContent.GetProperty("fAQCategory").GetValue();
                var test = GetPropertyName(faqList.FirstOrDefault(),"categoryName", "Name").Contains(searchVal.Trim().ToLower()); 
                faqList = faqList.Where(x => GetPropertyName(x, "categoryName", "Name").Contains(searchVal.Trim().ToLower()) || Convert.ToString(x.GetProperty("categoryHeading").GetValue()).ToLower().Contains(searchVal.Trim().ToLower())).ToList();

                var str = RenderPartialViewToString("_searchFaq", faqList);
                return Json(str, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Logger.Info<GamesController>("Error Message: " + e.Message + "\n Error Inner Exp: " + e.InnerException);
                return Json(null, JsonRequestBehavior.AllowGet);
            }

        }

        /// <summary>
        /// This Method is for Render the partial views
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="viewmodel"></param>
        /// <returns></returns>
        public virtual string RenderPartialViewToString(string viewName, object viewmodel)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = this.ControllerContext.RouteData.GetRequiredString("action");
            }

            ViewData.Model = viewmodel;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = System.Web.Mvc.ViewEngines.Engines.FindPartialView(this.ControllerContext, viewName);
                var viewContext = new ViewContext(this.ControllerContext, viewResult.View, this.ViewData, this.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);

                return sw.GetStringBuilder().ToString();
            }
        }

        /// <summary>
        /// This Method is for Add Games
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ParentId"></param>
        /// <param name="theorydata"></param>
        /// <returns></returns>

        public int AddGameinBackend(ApiModels.GameData data, int ParentId, ApiModels.GameListData theorydata)
        {
            List<ApiModels.GamePOEditorCaseTitleData> poCaseTitles = new List<ApiModels.GamePOEditorCaseTitleData>();
            if (data.POEditorCaseTitle != null)
            {
                poCaseTitles = JsonConvert.DeserializeObject<List<GamePOEditorCaseTitleData>>(data.POEditorCaseTitle);
            }

            var language = "de-de";
            var caseDescription = data.Description;
            var casetitle = data.Title;
            if (poCaseTitles.Count > 0)
            {
                var IsMatched = false;
                foreach (var titleInfo in poCaseTitles)
                {
                    if (titleInfo.code.ToLower() == "de-de")
                    {
                        casetitle = titleInfo.title;
                        language = titleInfo.code;
                        IsMatched = true;
                        break;
                    }
                }
                if (IsMatched == false)
                {
                    casetitle = poCaseTitles[0].title;
                    language = poCaseTitles[0].code;
                }
            }

            var theory = "";
            foreach (var tData in theorydata.Data)
            {
                if (data.ChoosenTheory.Contains(tData.Id))
                {
                    theory = theory + tData.Title + ", ";
                }
            }
            if (theory != "")
            {
                theory = theory.Substring(0, theory.Length - 2);
            }

            POEditorResponse response = new POEditorResponse();
            var request = new
            {
                id = data.POEditorProjectId,
                api_token = Convert.ToString(ConfigurationManager.AppSettings["API_Token"]),
                language = language
            };
            try
            {
                string APIUrl = "https://api.poeditor.com/v2/terms/list";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(APIUrl);
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var projectdata = string.Format("api_token={0}&id={1}&language={2}", request.api_token, request.id, request.language);
                    streamWriter.Write(projectdata);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    result = result.Replace("[AND]", "&");
                    response = JsonConvert.DeserializeObject<POEditorResponse>(result);
                    foreach (var item in response.result.terms)
                    {
                        if (item.context == "Case_Description")
                        {
                            caseDescription = item.translation.content;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                caseDescription = ex.Message;
            }


            var contentService = Services.ContentService;
            var app = contentService.Create(
               casetitle, // the name of the product
                ParentId, // the parent id should be the id of the group node 
                "casePage", // the alias of the product Document Type
                0);
            app.SetCultureName(casetitle, "de-DE");
            app.SetValue("addedByAPI", true,language);
            app.SetValue("caseTitle", casetitle, language);
            app.SetValue("heading", casetitle, language);
            app.SetValue("playedGame", data.TimesPlayed, language);
            app.SetValue("description", caseDescription, language);
            app.SetValue("uid", data.Id, language);
            app.SetValue("moduleName", data.ModuleName, language);
            app.SetValue("imageUrl", data.Picture);
            app.SetValue("pOEditorCaseTitle", data.POEditorCaseTitle, language);
            app.SetValue("caseDescription", data.WebsiteDescription);
            app.SetValue("length", data.Length, language);
            app.SetValue("language", data.Language, language);
            app.SetValue("theory", theory, language);
            app.SetValue("pOEditorProjectId", data.POEditorProjectId, language);
            app.SetValue("category", data.ModuleName, language);
            app.SetValue("isFreeGame", !(data.IsLocked), language);
            contentService.SaveAndPublish(app,language);
            return app.Id;
        }


        /// <summary>
        /// This Method is for update the Game
        /// </summary>
        /// <param name="data"></param>
        /// <param name="PageId"></param>
        /// <param name="theorydata"></param>
        public void UpdateGameinBackend(ApiModels.GameData data, int PageId, ApiModels.GameListData theorydata)
        {
            var contentService = Services.ContentService;
            var app = contentService.GetById(PageId);
            List<GamePOEditorCaseTitleData> poCaseTitles = data.POEditorCaseTitle != null ? JsonConvert.DeserializeObject<List<GamePOEditorCaseTitleData>>(data.POEditorCaseTitle) : new List<GamePOEditorCaseTitleData>();

            var language = "de-de";
            var caseDescription = data.Description;
            var casetitle = data.Title;
            if (poCaseTitles.Count > 0)
            {
                var IsMatched = false;
                foreach (var titleInfo in poCaseTitles)
                {
                    if (titleInfo.code.ToLower() == "de-de")
                    {
                        casetitle = titleInfo.title;
                        language = titleInfo.code;
                        IsMatched = true;
                        break;
                    }
                }
                if (IsMatched == false)
                {
                    casetitle = poCaseTitles[0].title;
                    language = poCaseTitles[0].code;
                }
            }

            var theory = "";
            foreach (var tData in theorydata.Data)
            {
                if (data.ChoosenTheory.Contains(tData.Id))
                {
                    theory = theory + tData.Title + ", ";
                }
            }
            if (theory != "")
            {
                theory = theory.Substring(0, theory.Length - 2);
            }

            POEditorResponse response = new POEditorResponse();
            var request = new
            {
                id = data.POEditorProjectId,
                api_token = Convert.ToString(ConfigurationManager.AppSettings["API_Token"]),
                language = language
            };
            try
            {
                string APIUrl = "https://api.poeditor.com/v2/terms/list";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(APIUrl);
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var projectdata = string.Format("api_token={0}&id={1}&language={2}", request.api_token, request.id, request.language);
                    streamWriter.Write(projectdata);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    result = result.Replace("[AND]", "&");
                    response = JsonConvert.DeserializeObject<POEditorResponse>(result);
                    foreach (var item in response.result.terms)
                    {
                        if (item.context == "Case_Description")
                        {
                            caseDescription = item.translation.content;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                caseDescription = ex.Message;
            }

            app.SetValue("caseTitle", casetitle);
            app.SetValue("heading", casetitle);
            app.SetValue("playedGame", data.TimesPlayed);
            app.SetValue("uid", data.Id);
            app.SetValue("moduleName", data.ModuleName);
            app.SetValue("imageUrl", data.Picture);
            app.SetValue("pOEditorCaseTitle", data.POEditorCaseTitle);
            app.SetValue("caseDescription", data.WebsiteDescription);
            app.SetValue("length", data.Length);
            app.SetValue("language", data.Language);
            app.SetValue("theory", theory);
            app.SetValue("pOEditorProjectId", data.POEditorProjectId);
            app.SetValue("category", data.ModuleName);
            app.SetValue("isFreeGame", !(data.IsLocked));
            contentService.SaveAndPublish(app);



        }

    }

    public class POEditorResponse
    {
        public POResponse response { get; set; }
        public POResult result { get; set; }
    }

    public class POResponse
    {
        public string status { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class POResult
    {
        public List<POLanguage> languages { get; set; }
        public List<POTerms> terms { get; set; }
    }

    public class POLanguage
    {
        public string name { get; set; }
        public string code { get; set; }
        public string translations { get; set; }
        public string percentage { get; set; }
        public DateTime? updated { get; set; }
    }

    public class POTerms
    {
        public string term { get; set; }
        public string context { get; set; }
        public string plural { get; set; }
        public DateTime? created { get; set; }
        public DateTime? updated { get; set; }
        public string reference { get; set; }
        public string comment { get; set; }
        public POTranslation translation { get; set; }
    }

    public class POTranslation
    {
        public string content { get; set; }
        public string fuzzy { get; set; }
        public DateTime? updated { get; set; }
    }
}