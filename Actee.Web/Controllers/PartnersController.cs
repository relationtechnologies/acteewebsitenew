﻿using Actee.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Http.Cors;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Composing;
using Umbraco.Core.Logging;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web.WebApi;

namespace Actee.Web.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PartnersController : UmbracoApiController
    {
        private readonly IContentTypeBaseServiceProvider _contentTypeBaseServiceProvider;
        private HttpWebRequest httpWebRequest;

        public PartnersController(IContentTypeBaseServiceProvider contentTypeBaseServiceProvider)
        {
            _contentTypeBaseServiceProvider = contentTypeBaseServiceProvider;
        }

        /// <summary>
        /// This method is for Language
        /// </summary>
        /// <param name="Lang"></param>
        /// <returns></returns>
        public static string GetLanguage(string Lang)
        {
            List<Actee.Web.Models.ApiModels.GameLanguageData> languages = new List<Actee.Web.Models.ApiModels.GameLanguageData>();
            if (!string.IsNullOrEmpty(Lang))
            {
                languages = JsonConvert.DeserializeObject<List<Actee.Web.Models.ApiModels.GameLanguageData>>(Lang);
            }
            var la = "";
            var newLan = "";
            foreach (var lan in languages)
            {
                if (!string.IsNullOrEmpty(lan.name))
                {
                    if (lan.state == "Ready")
                    {
                        la = la + lan.name + ",";
                    }

                }

            }
            if (la.Length > 0)
                newLan = la.Remove(la.Length - 1, 1);
            return newLan;
        }

        /// <summary>
        /// This Method is for create Partner
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        // GET: APIData
        [HttpPost]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public string Partner(PartnerHubModel model)
        {
            try
            {

                if (model != null)
                {
                    var languageCodeEn = "en-US";
                    var languageCodeDe = "de-DE";
                    if (!string.IsNullOrEmpty(model.LanguageCode) && (model.LanguageCode.Contains(languageCodeEn) || model.LanguageCode.Contains(languageCodeDe)))
                    {
                        languageCodeEn = model.LanguageCode;
                        languageCodeDe = model.LanguageCode;
                    }
                    var contentService = Current.Services.ContentService;
                    if (model.ActionType.ToLower() == "create" || model.ActionType.ToLower() == "update")
                    {
                        IContent PartnerHub = null;
                        if (model.ActionType.ToLower() == "create")
                        {
                            var apiNodeId = Convert.ToInt32(ConfigurationManager.AppSettings["PartnersPageId"]);
                            PartnerHub = contentService.Create(model.Name, apiNodeId, "partnerCards");
                            PartnerHub.Key = new Guid();
                        }
                        else
                        {
                            if (model.NodeId < 1)
                            {
                                return "Please provide node id !!";
                            }
                            PartnerHub = contentService.GetById(model.NodeId);
                        }
                        PartnerHub.SetCultureName(model.Name, languageCodeEn);
                        PartnerHub.SetCultureName(model.Name, languageCodeDe);
                        if (!string.IsNullOrEmpty(model.Logo))
                        {
                            var mediaNodeId = Convert.ToInt32(ConfigurationManager.AppSettings["MediaNodeId"]);
                            string[] image = model.Logo.Split('/');
                            if (image != null && image.Length > 0)
                            {
                                GuidUdi guidUdi = DownloadImage(model.Logo, image[image.Length - 1], mediaNodeId);
                                if (guidUdi != null)
                                {
                                    PartnerHub.SetValue("partnersImage", guidUdi.ToString(), languageCodeEn);
                                    PartnerHub.SetValue("partnersImage", guidUdi.ToString(), languageCodeDe);
                                }
                            }
                        }
                        PartnerHub.SetValue("partnerAddressLink", model.WebsiteName != null ? model.WebsiteName : string.Empty, languageCodeEn);
                        PartnerHub.SetValue("partnerAddressLink", model.WebsiteName != null ? model.WebsiteName : string.Empty, languageCodeDe);
                        PartnerHub.SetValue("partnerEmail", model.Email != null ? model.Email : string.Empty, languageCodeEn);
                        PartnerHub.SetValue("partnerEmail", model.Email != null ? model.Email : string.Empty, languageCodeDe);
                        PartnerHub.SetValue("contactNumber", model.ContactNumber != null ? model.ContactNumber : string.Empty, languageCodeEn);
                        PartnerHub.SetValue("contactNumber", model.ContactNumber != null ? model.ContactNumber : string.Empty, languageCodeDe);
                        PartnerHub.SetValue("partnerLanguages", model.Language != null ? model.Language : string.Empty, languageCodeEn);
                        PartnerHub.SetValue("partnerLanguages", model.Language != null ? model.Language : string.Empty, languageCodeDe);
                        PartnerHub.SetValue("partnerDescription", model.Description != null ? model.Description : string.Empty, languageCodeEn);
                        PartnerHub.SetValue("partnerDescription", model.Description != null ? model.Description : string.Empty, languageCodeDe);
                        PartnerHub.SetValue("partnerType", "Company", languageCodeEn);
                        PartnerHub.SetValue("partnerType", "Company", languageCodeDe);
                        PartnerHub.SetValue("isLogo", true, languageCodeEn);
                        PartnerHub.SetValue("isLogo", true, languageCodeDe);
                        PartnerHub.SetValue("partnerName", model.Name, languageCodeEn);
                        PartnerHub.SetValue("partnerName", model.Name, languageCodeDe);
                        contentService.SaveAndPublish(PartnerHub, languageCodeEn);
                        contentService.SaveAndPublish(PartnerHub, languageCodeDe);
                        return PartnerHub.Id.ToString();
                    }
                    else if (model.ActionType.ToLower() == "delete")
                    {
                        if (model.NodeId < 1)
                        {
                            return "Please provide node id !!";
                        }
                        var PartnerHub = contentService.GetById(model.NodeId);
                        contentService.Delete(PartnerHub);
                        return "Partner Hub delete successfully !!";
                    }
                    return "Please provide action type !!";
                }

                return "Please provide data for partner hub !!";

            }
            catch (Exception ex)
            {
                Logger.Info<PartnersController>("PartnerError When adding from API: " + ex.Message);
                return ex.Message;
            }
        }

        private GuidUdi DownloadImage(string imageUrl, string Name, int mediaParentId)
        {
            //return "";
            httpWebRequest = (HttpWebRequest)WebRequest.Create(imageUrl);
            httpWebRequest.Method = WebRequestMethods.Http.Get;
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
            var orignalPath = "";
            try
            {
                using (HttpWebResponse httpResponse = httpWebRequest.GetResponse() as HttpWebResponse)
                {
                    if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        Stream stream = httpResponse.GetResponseStream();
                        var memoryStream = new MemoryStream();
                        stream.CopyTo(memoryStream);
                        try
                        {
                            Name = Name.Replace(" ", "-");
                            orignalPath = "/img/" + Name;
                            IMediaService mediaService = Services.MediaService;
                            IMedia media = mediaService.CreateMedia(Name, mediaParentId, "Image");
                            media.SetValue(_contentTypeBaseServiceProvider, "umbracoFile", Name, memoryStream);
                            mediaService.Save(media);
                            return media.GetUdi();
                        }
                        catch
                        {
                            return null;
                        }
                    }
                }

            }
            catch
            {
                return null;
            }

            return null;
        }


        [HttpPost]
        public string CertifiedConsultant(Employee model)
        {
            string error = "0";
            try
            {
                if (model != null)
                {
                    error = "1";
                    var contentService = Services.ContentService;
                    error = "1.1";
                    if (model.ActionType.ToLower() == "create" || model.ActionType.ToLower() == "update")
                    {
                        error = "2";
                        IContent BlogData = null;
                        error = "2.1";
                        if (model.ActionType.ToLower() == "create")
                        {
                            var consultantsPageId = Convert.ToInt32(ConfigurationManager.AppSettings["ConsultantsPageId"]);
                            BlogData = contentService.Create(model.EmployeeName, consultantsPageId, "partnerCards");
                            BlogData.Key = new Guid();
                        }
                        else
                        {
                            BlogData = contentService.GetById(model.NodeId);
                            BlogData.Name = (model.EmployeeName != "" && model.EmployeeName != null) ? model.EmployeeName : BlogData.Name;
                        }
                        BlogData.SetCultureName(model.EmployeeName, "en-US");
                        List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
                        error = "3";
                        if (model.SocialMediaList != null && model.SocialMediaList.Count() > 0)
                        {
                            foreach (var item in model.SocialMediaList)
                            {
                                string mediaid = "";
                                var guidimage = "7b6ab2b5-735e-49cc-b77f-ecc0c2f63fac";

                                if (item.Name.ToLower().Contains("link"))
                                {
                                    guidimage = "7b6ab2b5-735e-49cc-b77f-ecc0c2f63fac";
                                }
                                else if (item.Name.ToLower().Contains("mail"))
                                {
                                    guidimage = "e25bc393-1770-4c90-8321-adb28020ec8c";
                                }

                                mediaid = string.Concat("umb://media/", guidimage.Replace("-", ""));

                                items.Add(new Dictionary<string, object>() {
                                    { "key", new Guid() },
                                    { "name", item.Name },
                                    { "ncContentTypeAlias", "socialMediaNC"},
                                    { "socialMediaName" , item.Name},
                                    { "iconImage", mediaid },
                                    { "iconLink", item.IconLink },
                                    { "iconHeading", item.IconHeading},
                                    { "iconDescription", item.IconDescription}
                                });
                            }
                        }

                        List<string> otherCategoriesList = new List<string>();

                        error = "4";
                        if (model.EmployeeType != null)
                        {
                            foreach (var item in model.EmployeeType)
                            {
                                long count;
                                var blogcat = contentService.GetPagedChildren(1407, 0, 1000, out count);
                                if (blogcat.Count() <= 0)
                                {
                                    blogcat = contentService.GetPagedChildren(3285, 0, 1000, out count);
                                }
                                foreach (var cat in blogcat)
                                {
                                    var guid = contentService.GetById(cat.Id).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }
                            }
                        }

                        int countryId = 0;

                        error = "5";
                        if (model.CountriesOfOperation != null)
                        {
                            foreach (var item in model.CountriesOfOperation)
                            {
                                long count2;
                                var countries = contentService.GetPagedChildren(2960, 0, 1000, out count2);
                                int isCountryExist = 0;
                                int catId = 0;
                                if (countries.Count() > 0)
                                {
                                    foreach (var cat in countries)
                                    {
                                        if (cat.Name == item.name)
                                        {
                                            catId = cat.Id;
                                            isCountryExist = 1;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    Country country = new Country();
                                    country.name = item.name ?? string.Empty;
                                    country.code = item.code ?? string.Empty;
                                    var guid = contentService.GetById(int.Parse(AddNewCountry(country))).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }

                                if (isCountryExist == 1)
                                {
                                    var guid = contentService.GetById(catId).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }
                                else
                                {
                                    Country country = new Country();
                                    country.name = item.name;
                                    country.code = item.code;

                                    countryId = int.Parse(AddNewCountry(country));

                                    var guid = contentService.GetById(countryId).GetUdi().Guid;
                                    otherCategoriesList.Add(string.Concat("umb://document/", guid.ToString().Replace("-", "")));
                                }
                            }
                        }
                        error = "6";
                        List<Dictionary<string, object>> conceptsList = new List<Dictionary<string, object>>();
                        if (model.ConceptCertifiedIn != null)
                        {
                            foreach (var conce in model.ConceptCertifiedIn)
                            {
                                if (conce.IsApproved)
                                {
                                    conceptsList.Add(new Dictionary<string, object>() {
                                        { "key", new Guid() },
                                        { "name", conce.name },
                                        { "ncContentTypeAlias", "conceptCertifiedNC"},
                                        { "conceptName", conce.name},
                                        { "value", conce.value},
                                        { "instructorName", conce.instructorName},
                                        { "certificationDate", conce.certificationDate},
                                        { "placeOfCertification", conce.placeOfCertification},
                                        { "approvedDate", conce.approvedDate}
                                    });
                                }
                            }
                        }

                        DateTime dateTime = new DateTime();

                        if (model.DateOfCertification != null)
                        {
                            dateTime = DateTime.Parse(model.DateOfCertification);
                        }

                        string[] image = model.EmployeeSmallImage != "" && model.EmployeeSmallImage != null ? model.EmployeeSmallImage.Split('/') : null;

                        var languageCode = "en-Us";
                        if (model.EmployeeName != "" && model.EmployeeName != null)
                        {
                            BlogData.Name = model.EmployeeName;
                            BlogData.SetValue("partnerName", model.EmployeeName, languageCode);
                        }
                        if (model.Desgination != "" && model.Desgination != null)
                            BlogData.SetValue("partnerAddress", model.Desgination, languageCode);
                        if (model.ContactNumber != "" && model.ContactNumber != null)
                            if (model.EmployeeSmallImage != "" && model.EmployeeSmallImage != null)
                            {
                                var mediaNodeId = Convert.ToInt32(ConfigurationManager.AppSettings["MediaNodeId"]);
                                GuidUdi guidUdi = DownloadImage(model.EmployeeSmallImage, image[image.Length - 1], mediaNodeId);
                                if (guidUdi != null)
                                {
                                    BlogData.SetValue("partnersImage", guidUdi.ToString(), languageCode);
                                }

                            }
                        if (items.Count() > 0 && items != null)
                            if (model.EmployeeDescription != "" && model.EmployeeDescription != null)
                                BlogData.SetValue("partnerDescription", model.EmployeeDescription, languageCode);
                        BlogData.SetValue("partnerType", "Consultant", languageCode);
                        if (model.Skills != "" && model.Skills != null)
                            BlogData.SetValue("skillsByAPI", model.Skills, languageCode);
                        contentService.SaveAndPublish(BlogData, languageCode);
                        return BlogData.Id.ToString();
                    }
                    else if (model.ActionType.ToLower() == "delete")
                    {
                        var BlogData = contentService.GetById(model.NodeId);
                        var consultantsPageId = Convert.ToInt32(ConfigurationManager.AppSettings["ConsultantsPageId"]);
                        if (BlogData.ContentType.Alias.Equals("partnerCards") && BlogData.ParentId.Equals(consultantsPageId))
                        {
                            contentService.Delete(BlogData);
                            return "Profile delete successfully !!";
                        }
                        else
                        {
                            return "nodeId is not correct.";
                        }
                    }
                    return "";
                }
                else
                {
                    return "Data not found";
                }
            }
            catch (Exception ex)
            {
                return " ActionType--" + model.ActionType
                    + " NodeId--" + model.NodeId
                    + " ConsultantType--" + model.ConsultantType
                    + " ContactNumber--" + model.ContactNumber
                    + " EmployeeSmallImage--" + model.EmployeeSmallImage
                    + " EmployeeName--" + model.EmployeeName
                    + " EmployeeDescription--" + model.EmployeeDescription
                    + "error==" + error + "----" + ex.Message + "-----" + ex.StackTrace + "---" + (ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        private string AddNewCountry(Country country)
        {
            try
            {
                var contentService = Services.ContentService;
                long children;
                var existNode = contentService.GetPagedChildren(2823, 0, 1000, out children).Where(x => x.GetValue<string>("locationName", "en-US").Equals(country.name)).FirstOrDefault();
                if (existNode == null)
                {
                    var BlogData = contentService.Create(country.name, 2823, "location");
                    if (country.name != "" && country.name != null)
                        BlogData.SetValue("locationName", country.name, "en-US");
                    BlogData.SetCultureName(country.name, "en-US");
                    contentService.SaveAndPublish(BlogData, "en-US");
                    return BlogData.Id.ToString();
                }
                else
                {
                    return existNode.Id.ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}