﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using static Actee.Web.Models.ApiModels;

namespace Actee.Web.Controllers
{
    public class PriceController : SurfaceController
    {
        public int AddUpdatePrice(List<Package> model)
        {
            //List<SessionAndLicenseLabel> SessionAndLicenseLabel = new List<SessionAndLicenseLabel>();

            List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

            //UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);
            //var PriceNode = helper.TypedContent(3520);
            //var content = PriceNode.GetPropertyValue<IEnumerable<IPublishedContent>>("questionPaper");

            var ContentService = Services.ContentService;
            var PriceNode = ContentService.GetById(3520);

            //var priceBox = PriceNode.GetValue<IEnumerable<IPublishedContent>>("priceBoxes");

            //if (priceBox != null)
            //{
            //    foreach (var item in priceBox)
            //    {
            //        SessionAndLicenseLabel.Add(new SessionAndLicenseLabel()
            //        {
            //            GameSessionLabel = item.GetPropertyValue<string>("usersPlayersLabel"),
            //            UsersPlayersLabel = item.GetPropertyValue<string>("gameSessionLabel")
            //        });
            //    }
            //}

            if (model != null)
            {
                int i = 0;
                foreach (var item in model)
                {
                    if (i++ == 0)
                    {
                        PriceNode.SetValue("tryItOutHeading", item.FullName);
                        PriceNode.SetValue("price", item.NetAmount);
                        PriceNode.SetValue("sessionQuantity", item.SessionQuantity);
                        PriceNode.SetValue("licenseQuantity", item.LicenseQuantity);
                        PriceNode.SetValue("packageID", item.Id);
                    }
                    else
                    {
                        string features = "<ul>";
                        if (item.packageFeature != null)
                        {
                            foreach (var feature in item.packageFeature)
                            {
                                features += "<li>" + feature.Feature + "</li>";
                            }
                            features += "</ul>";
                        }
                        data.Add(new Dictionary<string, object>(){
                        { "heading", item.FullName },
                        { "price", (item.NetAmount / 12) },
                        { "timeDuration", item.NetAmount },
                        { "packageID", item.Id },
                        { "sessionQuantity", (item.SessionQuantity >= 1000 ? "Unlimited" : item.SessionQuantity.ToString()) },
                        { "licenseQuantity", (item.LicenseQuantity >= 1000 ? "Unlimited" : item.LicenseQuantity.ToString()) },
                        { "details", features }
                    });
                    }
                }

                PriceNode.SetValue("priceBoxes", JsonConvert.SerializeObject(data));
                ContentService.SaveAndPublish(PriceNode);
                //umbraco.library.RefreshContent();
            }
            return 0;
        }
    }
}