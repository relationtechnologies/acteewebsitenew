﻿using Actee.Web.Controllers;
using Actee.Web.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Composing;
using static Actee.Web.Models.ApiModels;

namespace Actee.Web.Helpers
{
    public static class Extensions
    {

        /// <summary>
        /// Get Homepage node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static IPublishedContent GetHomepageNode(this IPublishedContent node)
        {
            return node.AncestorOrSelf(GlobalConstants.DocumentTypeAlias.Homepage);
        }
        public static string Encode(string encodeMe)
        {
            byte[] encoded = System.Text.Encoding.UTF8.GetBytes(encodeMe);
            return Convert.ToBase64String(encoded);
        }

        public static string Decode(string decodeMe)
        {
            byte[] encoded = Convert.FromBase64String(decodeMe);
            return System.Text.Encoding.UTF8.GetString(encoded);
        }
        /// <summary>
        /// Get parent node by level
        /// </summary>
        /// <param name="node"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public static IPublishedContent GetParentNodeByLevel(this IPublishedContent node, int level = 2)
        {
            return node.AncestorOrSelf(level);
        }

        /// <summary>
        /// Get Homepage node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>

        public static IPublishedContent GetNavigation(this IPublishedContent node)
        {
            return node.DescendantOrSelf(GlobalConstants.DocumentTypeAlias.Navigation);
        }
        public static IPublishedContent GetGlobleSettings(this IPublishedContent node)
        {
            return node.DescendantOrSelf(GlobalConstants.DocumentTypeAlias.GlobleSetting);
        }
        public static IPublishedContent GetCourses(this IPublishedContent node)
        {
            return node.DescendantOrSelf(GlobalConstants.DocumentTypeAlias.Courses);
        }
        public static IPublishedContent GetEmployee(this IPublishedContent node)
        {
            return node.DescendantOrSelf(GlobalConstants.DocumentTypeAlias.Employee);
        }
        public static IPublishedContent GetTools(this IPublishedContent node)
        {
            return node.DescendantOrSelf(GlobalConstants.DocumentTypeAlias.Tools);
        }
        public static IPublishedContent GetModelPopup(this IPublishedContent node)
        {
            return node.DescendantOrSelf(GlobalConstants.DocumentTypeAlias.ModelPopup);
        }
        public static IPublishedContent GetWebsiteConfiguration(this IPublishedContent node)
        {
            return node.DescendantOrSelf(GlobalConstants.DocumentTypeAlias.WebsiteConfiguration);
        }

        /// <summary>
        /// Get media url from media
        /// </summary>
        /// <param name="media"></param>
        /// <returns></returns>
        public static string GetMediaUrl(this IPublishedContent media)
        {
            try
            {
                if (media != null)
                {
                    return media.Value<string>("umbracoFile");
                }
            }
            catch { }
            return string.Empty;
        }

        /// <summary>
        /// Get media url from media
        /// </summary>
        /// <param name="media"></param>
        /// <returns></returns>
        public static string GetMediaUrl(int id)
        {
            UmbracoHelper helper = Current.UmbracoHelper;
            IPublishedContent media = helper.Media(id);
            return media.GetMediaUrl();
        }


        public static IEnumerable<IPublishedContent> GetNodeMultipleImage(this IPublishedContent node, string propertyName = "images")
        {
            if (node.HasValue(propertyName))
            {
                //UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);
                UmbracoHelper helper = Current.UmbracoHelper;
                var collectionList = node.Value<string>(propertyName).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);
                if (collectionList.Count() > 0)
                {
                    return helper.Media(collectionList);
                }
            }
            return null;
        }

        public static IEnumerable<IPublishedContent> GetMNTPContentNodes(this IPublishedContent node, string propertyName)
        {
            if (node.HasValue(propertyName))
            {
                var nodeIds = node.Value<string>(propertyName).Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);
                if (nodeIds != null && nodeIds.Count() > 0)
                {
                    //UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);
                    UmbracoHelper helper = Current.UmbracoHelper;
                    return helper.Content(nodeIds).Where(x => x != null);
                }
            }
            return null;
        }

        public static IEnumerable<IPublishedContent> GetMNTPContentNodes(string ids)
        {
            if (!string.IsNullOrEmpty(ids))
            {
                var nodeIds = ids.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse);
                if (nodeIds != null && nodeIds.Count() > 0)
                {
                    // UmbracoHelper helper = new UmbracoHelper(UmbracoContext.Current);
                    UmbracoHelper helper = Current.UmbracoHelper;
                    return helper.Content(nodeIds).Where(x => x != null);
                }
            }
            return null;
        }

        public static List<IPublishedContent> GetAllBlogpost(this IPublishedContent node)
        {
            return node.Descendants(GlobalConstants.DocumentTypeAlias.BlogPost).ToList();
        }
        public static List<IPublishedContent> GetAllVideoBlogpost(this IPublishedContent node)
        {
            return node.Descendants(GlobalConstants.DocumentTypeAlias.VideoBlogPost).ToList();
        }
        public static List<IPublishedContent> GetAllPressBlogpost(this IPublishedContent node)
        {
            return node.Descendants(GlobalConstants.DocumentTypeAlias.BlogPost).Where(a => a.Name == "Press Releases").ToList();
        }
        public static List<IPublishedContent> GetAllMediaCoverageBlogpost(this IPublishedContent node)
        {
            return node.Descendants(GlobalConstants.DocumentTypeAlias.BlogPost).Where(a => a.Name == "Media Coverage").ToList();
        }

        #region Related Links

        public static IEnumerable<Actee.Web.Models.RelatedLink> GetRelatedLinks(this IPublishedContent node, string propertyName)
        {
            if (node.HasValue(propertyName))
            {
                var data = node.Value<string>(propertyName);
                return JsonConvert.DeserializeObject<IEnumerable<Actee.Web.Models.RelatedLink>>(data);
            }
            return null;
        }

        public static Actee.Web.Models.RelatedLink GetSingleRelatedLinks(this IPublishedContent node, string propertyName)
        {
            if (node.HasValue(propertyName))
            {
                var data = node.Value<string>(propertyName);
                return JsonConvert.DeserializeObject<IEnumerable<RelatedLink>>(data).FirstOrDefault();
            }
            return null;
        }

        public static RelatedLink GetVortoSingleRelatedLinks(this IPublishedContent node, string propertyName, string cultureName)
        {
            if (node.HasValue(propertyName))
            {
                var data = node.Value<string>(propertyName, cultureName);
                return JsonConvert.DeserializeObject<IEnumerable<RelatedLink>>(data).FirstOrDefault();
            }
            return null;
        }

        public static string GetRelatedLinkUrl(this RelatedLink link)
        {
            if (link.IsInternal)
            {
                var node = link.GetInternalNode();
                return node != null ? node.Url() : string.Empty;
            }

            return link.Link;
        }

        public static IPublishedContent GetInternalNode(this RelatedLink link)
        {
            if (link.IsInternal)
            {
                UmbracoHelper helper = Current.UmbracoHelper;
                return helper.Content(link.Internal);
            }

            return null;
        }

        #endregion

        public static IPublishedContent GetClient(this IPublishedContent node)
        {
            return node.Descendant("Clients");
        }

        public static List<IPublishedContent> GetCourses(this IPublishedContent node, int skipRecords, int noOfRecords)
        {
            return node.Children.Skip(skipRecords).Take(noOfRecords).ToList();
        }
        public static List<IPublishedContent> GetClients(this IPublishedContent node, int skipRecords, int noOfRecords)
        {
            return node.Children.Skip(skipRecords).Take(noOfRecords).ToList();
        }

        public static List<IPublishedContent> GetBlogs(this IPublishedContent node, int skipRecords, int noOfRecords, string filter)
        {
            var AllNodes = node.Descendants(GlobalConstants.DocumentTypeAlias.BlogPost).ToList();
            var newsNodes = node.Descendants(GlobalConstants.DocumentTypeAlias.BlogPost).ToList();
            var PressNodes = node.Descendants(GlobalConstants.DocumentTypeAlias.BlogPost).Where(a => a.Parent.Name == "Press Releases").ToList();
            var MediaCoverageNodes = node.Descendants(GlobalConstants.DocumentTypeAlias.BlogPost).Where(a => a.Parent.Name == "Media Coverage").ToList();
            var videoNodes = node.Descendants(GlobalConstants.DocumentTypeAlias.VideoBlogPost).ToList();
            var allNodes = new List<IPublishedContent>();
            if (filter == "Video")
            {
                allNodes = videoNodes;

            }
            else if (filter == "News")
            {
                allNodes = newsNodes;
            }
            else if (filter == "Press Releases")
            {
                allNodes = PressNodes;
            }
            else if (filter == "Media Coverage")
            {
                allNodes = MediaCoverageNodes;
            }
            else
            {
                allNodes = AllNodes.Concat(videoNodes).ToList();
            }
            return allNodes.Skip(skipRecords).Take(noOfRecords).ToList();
        }

        public static List<IPublishedContent> GetAllGames(this IPublishedContent node, string search = "")
        {
            List<IPublishedContent> App = new List<IPublishedContent>();
            foreach (IPublishedContent item in node.Children)
            {
                App.Add(item);
            }
            return App;
        }
        public static Actee.Web.Models.ApiModels.APIAccessData GetApiAccessData()
        {
            Actee.Web.Models.ApiModels.APIAccessData data = new Actee.Web.Models.ApiModels.APIAccessData();
            data.apiUrl = "https://api.actee.com/";
            return data;
        }
        public static string GetLanguage(string Lang)
        {
            List<Actee.Web.Models.ApiModels.GameLanguageData> languages = new List<Actee.Web.Models.ApiModels.GameLanguageData>();
            if (!string.IsNullOrEmpty(Lang))
            {
                languages = JsonConvert.DeserializeObject<List<Actee.Web.Models.ApiModels.GameLanguageData>>(Lang);
            }
            var la = "";
            var newLan = "";
            foreach (var lan in languages)
            {
                if (!string.IsNullOrEmpty(lan.name))
                {
                    if (lan.state == "Ready")
                    {
                        la = la + lan.name + ",";
                    }

                }

            }
            if (la.Length > 0)
                newLan = la.Remove(la.Length - 1, 1);
            return newLan;
        }

        /// <summary>
        /// This method is for update the game 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="PageId"></param>
        /// <param name="theorydata"></param>
        /// <param name="umbracoContextFactory"></param>
        public static void UpdateGameinBackend(ApiModels.GameData data, int PageId, ApiModels.GameListData theorydata, IUmbracoContextFactory umbracoContextFactory)
        {
            var contentService = Current.Services.ContentService;
            var app = contentService.GetById(PageId);
            List<GamePOEditorCaseTitleData> poCaseTitles = data.POEditorCaseTitle != null ? JsonConvert.DeserializeObject<List<GamePOEditorCaseTitleData>>(data.POEditorCaseTitle) : new List<GamePOEditorCaseTitleData>();

            var language = "de-de";
            var caseDescription = data.Description;
            var casetitle = data.Title;
            if (poCaseTitles.Count > 0)
            {
                var IsMatched = false;
                foreach (var titleInfo in poCaseTitles)
                {
                    if (titleInfo.code.ToLower() == "de-de")
                    {
                        casetitle = titleInfo.title;
                        language = titleInfo.code;
                        IsMatched = true;
                        break;
                    }
                }
                if (IsMatched == false)
                {
                    casetitle = poCaseTitles[0].title;
                    language = poCaseTitles[0].code;
                }
            }

            var theory = "";
            foreach (var tData in theorydata.Data)
            {
                if (data.ChoosenTheory.Contains(tData.Id))
                {
                    theory = theory + tData.Title + ", ";
                }
            }
            if (theory != "")
            {
                theory = theory.Substring(0, theory.Length - 2);
            }

            POEditorResponse response = new POEditorResponse();
            var request = new
            {
                id = data.POEditorProjectId,
                api_token = Convert.ToString(ConfigurationManager.AppSettings["API_Token"]),
                language = language
            };
            try
            {
                string APIUrl = "https://api.poeditor.com/v2/terms/list";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(APIUrl);
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var projectdata = string.Format("api_token={0}&id={1}&language={2}", request.api_token, request.id, request.language);
                    streamWriter.Write(projectdata);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    result = result.Replace("[AND]", "&");
                    response = JsonConvert.DeserializeObject<POEditorResponse>(result);
                    if (!response.response.status.Equals("fail"))
                    {
                        foreach (var item in response.result?.terms)
                        {
                            if (item.context == "Case_Description")
                            {
                                caseDescription = item.translation.content;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                caseDescription = ex.Message;
            }

            var languageEN = "en-US";
            var languageDE = "de-DE";
                app.SetCultureName(casetitle, languageEN);
                app.SetCultureName(casetitle, languageDE);
                app.SetValue("addedByAPI", true, languageEN);
                app.SetValue("addedByAPI", true, languageDE);
                app.SetValue("gameHeading", casetitle, languageEN);
                app.SetValue("gameHeading", casetitle, languageDE);
                app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageEN);
                app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageDE);
                app.SetValue("uid", data.Id, languageEN);
                app.SetValue("uid", data.Id, languageDE);
                app.SetValue("imageUrl", data.Picture, languageEN);
                app.SetValue("imageUrl", data.Picture, languageDE);
                app.SetValue("pOEditorCaseTitle", data.POEditorCaseTitle, languageEN);
                app.SetValue("pOEditorCaseTitle", data.POEditorCaseTitle, languageDE);
                app.SetValue("caseDescription", data.WebsiteDescription, languageEN);
                app.SetValue("caseDescription", data.WebsiteDescription, languageDE);
                app.SetValue("length", data.Length, languageEN);
                app.SetValue("length", data.Length, languageDE);
                app.SetValue("language", data.Language, languageEN);
                app.SetValue("language", data.Language, languageDE);
                app.SetValue("theory", theory, languageEN);
                app.SetValue("theory", theory, languageDE);
                app.SetValue("pOEditorProjectId", data.POEditorProjectId, languageEN);
                app.SetValue("pOEditorProjectId", data.POEditorProjectId, languageDE);
                app.SetValue("isFreeGame", !(data.IsLocked), languageEN);
                app.SetValue("isFreeGame", !(data.IsLocked), languageDE);
                app.SetValue("gameUserName", data.UserName ?? string.Empty, languageEN);
                app.SetValue("gameUserName", data.UserName ?? string.Empty, languageDE);
                app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageEN);
                app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageDE);

                using (var contextReference = umbracoContextFactory.EnsureUmbracoContext())
                {
                    contentService.SaveAndPublish(app, languageEN);
                    contentService.SaveAndPublish(app, languageDE);
                }
        }


        /// <summary>
        /// This method is for update Quizgame
        /// </summary>
        /// <param name="data"></param>
        /// <param name="PageId"></param>
        /// <param name="umbracoContextFactory"></param>
        public static void UpdateGameinBackendForQuiz(Quiz data, int PageId, IUmbracoContextFactory umbracoContextFactory)
        {
            var contentService = Current.Services.ContentService;
            var app = contentService.GetById(PageId);
            var caseDescription = data.FullDescription;
            var casetitle = String.Empty;
            var listOfnames = new List<NameData>();
            try
            {
                var nameData = JsonConvert.DeserializeObject<NameData>(data.Name);
                casetitle = nameData.title;
            }
            catch
            {
                var nameData = JsonConvert.DeserializeObject<List<NameData>>(data.Name);
                listOfnames.AddRange(nameData);
            }
            var languageEN = "en-US";
            var languageDE = "de-DE";
            var caseTitleDe = casetitle;
            var enName = casetitle;
            var deName = casetitle;
            if (data.IsActive == true && data.PlatformId == "00000000-0000-0000-0000-000000000000")
            {

                if(listOfnames.Count() > 0)
                {
                    var enNameval = listOfnames.Where(x => x.code.ToLower() == "en-us").FirstOrDefault();
                    var deNameval = listOfnames.Where(x => x.code.ToLower() == "de-de").FirstOrDefault();
                    if(enNameval != null)
                    {
                        enName = enNameval.title;
                    }
                    if (deNameval != null)
                    {
                        deName = deNameval.title;
                    }
                }
                app.SetCultureName(enName, languageEN);
                app.SetCultureName(deName, languageDE);
                app.SetValue("addedByAPI", true, languageEN);
                app.SetValue("addedByAPI", true, languageDE);
                app.SetValue("gameHeading", enName, languageEN);
                app.SetValue("gameHeading", deName, languageDE);
                app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageEN);
                app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageDE);
                app.SetValue("uid", data.Id, languageEN);
                app.SetValue("uid", data.Id, languageDE);
                app.SetValue("imageUrl", data.Picture, languageEN);
                app.SetValue("imageUrl", data.Picture, languageDE);
                // app.SetValue("pOEditorCaseTitle", data.POEditorCaseTitle, languageEN);
                // app.SetValue("pOEditorCaseTitle", data.POEditorCaseTitle, languageDE);
                app.SetValue("caseDescription", data.WebsiteDescription, languageEN);
                app.SetValue("caseDescription", data.WebsiteDescription, languageDE);
                app.SetValue("length", data.Length, languageEN);
                app.SetValue("length", data.Length, languageDE);
                app.SetValue("language", data.Language, languageEN);
                app.SetValue("language", data.Language, languageDE);
                //  app.SetValue("theory", theory, languageEN);
                //  app.SetValue("theory", theory, languageDE);
                //  app.SetValue("pOEditorProjectId", data.POEditorProjectId, languageEN);
                //  app.SetValue("pOEditorProjectId", data.POEditorProjectId, languageDE);
                app.SetValue("isFreeGame", data.IsActive, languageEN);
                app.SetValue("isFreeGame", data.IsActive, languageDE);
                app.SetValue("gameUserName", data.UserName ?? string.Empty, languageEN);
                app.SetValue("gameUserName", data.UserName ?? string.Empty, languageDE);
                app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageEN);
                app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageDE);

                using (var contextReference = umbracoContextFactory.EnsureUmbracoContext())
                {
                    contentService.SaveAndPublish(app, languageEN);
                    contentService.SaveAndPublish(app, languageDE);
                }
            }

        }

        /// <summary>
        /// This method is for add Game 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ParentId"></param>
        /// <param name="theorydata"></param>
        /// <param name="umbracoContextFactory"></param>
        /// <returns></returns>
        public static int AddGameinBackend(ApiModels.GameData data, int ParentId, ApiModels.GameListData theorydata, IUmbracoContextFactory umbracoContextFactory)
        {
            try
            {
                var languageCodeEN = "en-US";
                var languageCodeDE = "de-DE";
                if (!string.IsNullOrEmpty(data.LanguageCode) && (data.LanguageCode.Contains(languageCodeEN) || data.LanguageCode.Contains(languageCodeDE)))
                {
                    languageCodeEN = data.LanguageCode;
                    languageCodeDE = data.LanguageCode;
                }

                List<ApiModels.GamePOEditorCaseTitleData> poCaseTitles = new List<ApiModels.GamePOEditorCaseTitleData>();
                if (data.POEditorCaseTitle != null)
                {
                    poCaseTitles = JsonConvert.DeserializeObject<List<GamePOEditorCaseTitleData>>(data.POEditorCaseTitle);
                }

                var language = "de-de";
                var caseDescription = data.Description;
                var casetitle = data.Title;
                if (poCaseTitles.Count > 0)
                {
                    var IsMatched = false;
                    foreach (var titleInfo in poCaseTitles)
                    {
                        if (titleInfo.code.ToLower() == "en-us")//as it is
                        {
                            casetitle = titleInfo.title;
                            language = titleInfo.code;
                            IsMatched = true;
                            break;
                        }
                    }
                    if (IsMatched == false)
                    {
                        casetitle = poCaseTitles[0].title;
                        language = poCaseTitles[0].code;
                    }
                }

                var theory = "";
                foreach (var tData in theorydata.Data)
                {
                    if (data.ChoosenTheory.Contains(tData.Id))
                    {
                        theory = theory + tData.Title + ", ";
                    }
                }
                if (theory != "")
                {
                    theory = theory.Substring(0, theory.Length - 2);
                }

                POEditorResponse response = new POEditorResponse();
                var request = new
                {
                    id = data.POEditorProjectId,
                    api_token = Convert.ToString(ConfigurationManager.AppSettings["API_Token"]),
                    language = language
                };
                try
                {
                    string APIUrl = "https://api.poeditor.com/v2/terms/list";
                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(APIUrl);
                    httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                    httpWebRequest.Method = "POST";
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        var projectdata = string.Format("api_token={0}&id={1}&language={2}", request.api_token, request.id, request.language);
                        streamWriter.Write(projectdata);
                        streamWriter.Flush();
                    }

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        result = result.Replace("[AND]", "&");
                        response = JsonConvert.DeserializeObject<POEditorResponse>(result);
                        if (response != null && response.response?.status != "fail")
                        {
                            foreach (var item in response.result.terms)
                            {
                                if (item.context == "Case_Description")
                                {
                                    caseDescription = item.translation.content;
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    caseDescription = ex.Message;
                }


                var contentService = Current.Services.ContentService;
                long totalRecords;
                var gameCategoryPage = contentService.GetPagedChildren(ParentId, 0, 1000, out totalRecords).Where(x => !string.IsNullOrEmpty(x.GetValue<string>("category","en-US")) && x.GetValue<string>("category","en-US").ToLower().Equals(data.ModuleName.ToLower())).FirstOrDefault();
                    if (gameCategoryPage != null)
                    {
                        var app = contentService.Create(
                                       casetitle, // the name of the product
                                        gameCategoryPage.Id, // the parent id should be the id of the group node 
                                        "singleGame", // the alias of the product Document Type
                                        0);
                        app.SetCultureName(casetitle, languageCodeEN);
                        app.SetCultureName(casetitle, languageCodeDE);
                        app.SetValue("addedByAPI", true, languageCodeEN);
                        app.SetValue("addedByAPI", true, languageCodeDE);
                        app.SetValue("gameHeading", casetitle, languageCodeEN);
                        app.SetValue("gameHeading", casetitle, languageCodeDE);
                        app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageCodeEN);
                        app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageCodeDE);
                        app.SetValue("uid", data.Id, languageCodeEN);
                        app.SetValue("uid", data.Id, languageCodeDE);
                        app.SetValue("imageUrl", data.Picture, languageCodeEN);
                        app.SetValue("imageUrl", data.Picture, languageCodeDE);
                        app.SetValue("pOEditorCaseTitle", data.POEditorCaseTitle, languageCodeEN);
                        app.SetValue("pOEditorCaseTitle", data.POEditorCaseTitle, languageCodeDE);
                        app.SetValue("caseDescription", data.WebsiteDescription, languageCodeEN);
                        app.SetValue("caseDescription", data.WebsiteDescription, languageCodeDE);
                        app.SetValue("length", data.Length, languageCodeEN);
                        app.SetValue("length", data.Length, languageCodeDE);
                        app.SetValue("language", data.Language, languageCodeEN);
                        app.SetValue("language", data.Language, languageCodeDE);
                        app.SetValue("theory", theory, languageCodeEN);
                        app.SetValue("theory", theory, languageCodeDE);
                        app.SetValue("pOEditorProjectId", data.POEditorProjectId, languageCodeEN);
                        app.SetValue("pOEditorProjectId", data.POEditorProjectId, languageCodeDE);
                        app.SetValue("isFreeGame", !(data.IsLocked), languageCodeEN);
                        app.SetValue("isFreeGame", !(data.IsLocked), languageCodeDE);
                        app.SetValue("gameUserName", data.UserName ?? string.Empty, languageCodeEN);
                        app.SetValue("gameUserName", data.UserName ?? string.Empty, languageCodeDE);
                        app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageCodeEN);
                        app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageCodeDE);

                        using (var contextReference = umbracoContextFactory.EnsureUmbracoContext())
                        {
                            contentService.SaveAndPublish(app, languageCodeEN);
                            contentService.SaveAndPublish(app, languageCodeDE);
                        }
                        return app.Id;

                    }
                    else
                    {
                        return -1;
                    }

                }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        /// <summary>
        /// This method is for add Quizgame
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ParentId"></param>
        /// <param name="umbracoContextFactory"></param>
        /// <returns></returns>
        public static int AddGameinBackendForQuiz(Quiz data, int ParentId, IUmbracoContextFactory umbracoContextFactory)
        {
            try
            {
                var languageCodeEN = "en-US";
                var languageCodeDE = "de-DE";
                var caseDescription = data.FullDescription;
                var casetitle = String.Empty;
                var listOfnames = new List<NameData>();
                try
                {
                    var nameData = JsonConvert.DeserializeObject<NameData>(data.Name);
                    casetitle = nameData.title;
                }
                catch
                {
                    var nameData = JsonConvert.DeserializeObject<List<NameData>>(data.Name);
                    listOfnames.AddRange(nameData);
                    casetitle = nameData.FirstOrDefault().title;
                }
                var contentService = Current.Services.ContentService;
                long totalRecords;
                var gameCategoryPage = contentService.GetPagedChildren(ParentId, 0, 1000, out totalRecords).Where(x => !string.IsNullOrEmpty(x.GetValue<string>("category","en-Us")) && x.GetValue<string>("category","en-Us").ToLower().Equals("quiz")).FirstOrDefault();
                if (data.IsActive == true && data.PlatformId == "00000000-0000-0000-0000-000000000000")
                {
                    if (gameCategoryPage != null)
                    {
                        var app = contentService.Create(
                                       casetitle, // the name of the product
                                        gameCategoryPage.Id, // the parent id should be the id of the group node 
                                        "singleGame", // the alias of the product Document Type
                                        0);
                        var caseTitleDe = casetitle;
                        var enName = casetitle;
                        var deName = casetitle;
                        if (listOfnames.Count() > 0)
                        {
                            var enNameval = listOfnames.Where(x => x.code.ToLower() == "en-us").FirstOrDefault();
                            var deNameval = listOfnames.Where(x => x.code.ToLower() == "de-de").FirstOrDefault();
                            if (enNameval != null)
                            {
                                enName = enNameval.title;
                            }
                            if (deNameval != null)
                            {
                                deName = deNameval.title;
                            }
                        }
                        app.SetCultureName(enName, languageCodeEN);
                        app.SetCultureName(deName, languageCodeDE);
                        app.SetValue("addedByAPI", true, languageCodeEN);
                        app.SetValue("addedByAPI", true, languageCodeDE);
                        app.SetValue("gameHeading", enName, languageCodeEN);
                        app.SetValue("gameHeading", deName, languageCodeDE);
                        app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageCodeEN);
                        app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageCodeDE);
                        app.SetValue("uid", data.Id, languageCodeEN);
                        app.SetValue("uid", data.Id, languageCodeDE);
                        app.SetValue("imageUrl", data.Picture, languageCodeEN);
                        app.SetValue("imageUrl", data.Picture, languageCodeDE);
                        app.SetValue("caseDescription", data.WebsiteDescription, languageCodeEN);
                        app.SetValue("caseDescription", data.WebsiteDescription, languageCodeDE);
                        app.SetValue("length", data.Length, languageCodeEN);
                        app.SetValue("length", data.Length, languageCodeDE);
                        app.SetValue("language", data.Language, languageCodeEN);
                        app.SetValue("language", data.Language, languageCodeDE);
                        app.SetValue("isFreeGame", data.IsActive, languageCodeEN);
                        app.SetValue("isFreeGame", data.IsActive, languageCodeDE);
                        app.SetValue("gameUserName", data.UserName ?? string.Empty, languageCodeEN);
                        app.SetValue("gameUserName", data.UserName ?? string.Empty, languageCodeDE);
                        app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageCodeEN);
                        app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageCodeDE);

                        using (var contextReference = umbracoContextFactory.EnsureUmbracoContext())
                        {
                            contentService.SaveAndPublish(app, languageCodeEN);
                            contentService.SaveAndPublish(app, languageCodeDE);
                        }
                        return app.Id;

                    }
                }
                return -1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// This method is for add Styletradegame  
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ParentId"></param>
        /// <param name="umbracoContextFactory"></param>
        /// <returns></returns>
        public static int AddGameinBackendForStyletrade(Quiz data, int ParentId, IUmbracoContextFactory umbracoContextFactory)
        {
            try
            {
                var languageCodeEN = "en-US";
                var languageCodeDE = "de-DE";
                var caseDescription = data.FullDescription;
                var casetitle = String.Empty;
                var listOfnames = new List<NameData>();
                try
                {
                    var nameData = JsonConvert.DeserializeObject<NameData>(data.Name);
                    casetitle = nameData.title;
                }
                catch
                {
                    var nameData = JsonConvert.DeserializeObject<List<NameData>>(data.Name);
                    listOfnames.AddRange(nameData);
                    casetitle = nameData.FirstOrDefault().title;
                }
                var contentService = Current.Services.ContentService;
                long totalRecords;
                var gameCategoryPage = contentService.GetPagedChildren(ParentId, 0, 1000, out totalRecords).Where(x => !string.IsNullOrEmpty(x.GetValue<string>("category", "en-Us")) && x.GetValue<string>("category", "en-Us").ToLower().Equals("styletrade")).FirstOrDefault();
                if (data.IsActive == true && data.PlatformId == "00000000-0000-0000-0000-000000000000")
                {
                    if (gameCategoryPage != null)
                    {
                        var app = contentService.Create(
                                       casetitle, // the name of the product
                                        gameCategoryPage.Id, // the parent id should be the id of the group node 
                                        "singleGame", // the alias of the product Document Type
                                        0);
                        var caseTitleDe = casetitle;
                        var enName = casetitle;
                        var deName = casetitle;
                        if (listOfnames.Count() > 0)
                        {
                            var enNameval = listOfnames.Where(x => x.code.ToLower() == "en-us").FirstOrDefault();
                            var deNameval = listOfnames.Where(x => x.code.ToLower() == "de-de").FirstOrDefault();
                            if (enNameval != null)
                            {
                                enName = enNameval.title;
                            }
                            if (deNameval != null)
                            {
                                deName = deNameval.title;
                            }
                        }
                        app.SetCultureName(enName, languageCodeEN);
                        app.SetCultureName(deName, languageCodeDE);
                        app.SetValue("addedByAPI", true, languageCodeEN);
                        app.SetValue("addedByAPI", true, languageCodeDE);
                        app.SetValue("gameHeading", enName, languageCodeEN);
                        app.SetValue("gameHeading", deName, languageCodeDE);
                        app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageCodeEN);
                        app.SetValue("playedGame", $"Played : {data.TimesPlayed} Times", languageCodeDE);
                        app.SetValue("uid", data.Id, languageCodeEN);
                        app.SetValue("uid", data.Id, languageCodeDE);
                        app.SetValue("imageUrl", data.Picture, languageCodeEN);
                        app.SetValue("imageUrl", data.Picture, languageCodeDE);
                        app.SetValue("caseDescription", data.WebsiteDescription, languageCodeEN);
                        app.SetValue("caseDescription", data.WebsiteDescription, languageCodeDE);
                        app.SetValue("length", data.Length, languageCodeEN);
                        app.SetValue("length", data.Length, languageCodeDE);
                        app.SetValue("language", data.Language, languageCodeEN);
                        app.SetValue("language", data.Language, languageCodeDE);
                        app.SetValue("isFreeGame", data.IsActive, languageCodeEN);
                        app.SetValue("isFreeGame", data.IsActive, languageCodeDE);
                        app.SetValue("gameUserName", data.UserName ?? string.Empty, languageCodeEN);
                        app.SetValue("gameUserName", data.UserName ?? string.Empty, languageCodeDE);
                        app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageCodeEN);
                        app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageCodeDE);

                        using (var contextReference = umbracoContextFactory.EnsureUmbracoContext())
                        {
                            contentService.SaveAndPublish(app, languageCodeEN);
                            contentService.SaveAndPublish(app, languageCodeDE);
                        }
                        return app.Id;

                    }
                }
                return -1;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// This method is for update Styletrade game
        /// </summary>
        /// <param name="data"></param>
        /// <param name="PageId"></param>
        /// <param name="umbracoContextFactory"></param>
        public static void UpdateGameinBackendForStyletrade(Quiz data, int PageId, IUmbracoContextFactory umbracoContextFactory)
        {
            var contentService = Current.Services.ContentService;
            var app = contentService.GetById(PageId);
            var caseDescription = data.FullDescription;
            var casetitle = String.Empty;
            var listOfnames = new List<NameData>();
            try
            {
                var nameData = JsonConvert.DeserializeObject<NameData>(data.Name);
                casetitle = nameData.title;
            }
            catch
            {
                var nameData = JsonConvert.DeserializeObject<List<NameData>>(data.Name);
                listOfnames.AddRange(nameData);
            }
            var languageEN = "en-US";
            var languageDE = "de-DE";
            var caseTitleDe = casetitle;
            var enName = casetitle;
            var deName = casetitle;
            if (data.IsActive == true && data.PlatformId== "00000000-0000-0000-0000-000000000000")
            {
                if (listOfnames.Count() > 0)
                {
                    var enNameval = listOfnames.Where(x => x.code.ToLower() == "en-us").FirstOrDefault();
                    var deNameval = listOfnames.Where(x => x.code.ToLower() == "de-de").FirstOrDefault();
                    if (enNameval != null)
                    {
                        enName = enNameval.title;
                    }
                    if (deNameval != null)
                    {
                        deName = deNameval.title;
                    }
                }
                app.SetCultureName(enName, languageEN);
                app.SetCultureName(deName, languageDE);
                app.SetValue("addedByAPI", true, languageEN);
                app.SetValue("addedByAPI", true, languageDE);
                app.SetValue("gameHeading", enName, languageEN);
                app.SetValue("gameHeading", deName, languageDE);
                app.SetValue("playedGame",$"Played : {data.TimesPlayed} Times", languageEN);
                app.SetValue("playedGame",$"Played : {data.TimesPlayed} Times", languageDE);
                app.SetValue("uid", data.Id, languageEN);
                app.SetValue("uid", data.Id, languageDE);
                app.SetValue("imageUrl", data.Picture, languageEN);
                app.SetValue("imageUrl", data.Picture, languageDE);
                app.SetValue("caseDescription", data.WebsiteDescription, languageEN);
                app.SetValue("caseDescription", data.WebsiteDescription, languageDE);
                app.SetValue("length", data.Length, languageEN);
                app.SetValue("length", data.Length, languageDE);
                app.SetValue("language", data.Language, languageEN);
                app.SetValue("language", data.Language, languageDE);
                app.SetValue("isFreeGame", data.IsActive, languageEN);
                app.SetValue("isFreeGame", data.IsActive, languageDE);
                app.SetValue("gameUserName", data.UserName ?? string.Empty, languageEN);
                app.SetValue("gameUserName", data.UserName ?? string.Empty, languageDE);
                app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageEN);
                app.SetValue("createImageUrl", data.UserProfile ?? string.Empty, languageDE);

                using (var contextReference = umbracoContextFactory.EnsureUmbracoContext())
                {
                    contentService.SaveAndPublish(app, languageEN);
                    contentService.SaveAndPublish(app, languageDE);
                }
            }
        }

        /// <summary>
        /// Get access token
        /// </summary>
        /// <returns></returns>
        public static string GetAccessToken()
        {
            TokenData tokenData = new TokenData();
            APIAccessData apiData = GetApiAccessData();

            var httpWebRequest = ((HttpWebRequest)WebRequest.Create(apiData.apiUrl + "/User/GenerateAuthenticationToken"));
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            var user = Convert.ToString(ConfigurationManager.AppSettings["API_UserId"]);
            var pass = Convert.ToString(ConfigurationManager.AppSettings["API_UserPassword"]);
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                var url = apiData.apiUrl;
                string json = "{\"Username\": \"" + user + "\",\"Password\": \"" + pass + "\",\"APIURL\": \"" + url + "\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                var responseData = JsonConvert.DeserializeObject<TokenData>(result);
                if (responseData != null && responseData.Data != null)
                {
                    return responseData.Data.access_token;
                }
            }

            return string.Empty;
        }
        
        /// <summary>
        /// This method is for Add all games
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="_contentService"></param>
        /// <param name="umbracoContextFactory"></param>
        /// <returns></returns>
        public static GameListData GetAndAddAllGamesinBackend(int nodeId, IContentService _contentService, IUmbracoContextFactory umbracoContextFactory)
        {
            GameListData appdata = new GameListData();
            GameListData theorydata = new GameListData();
            APIAccessData apiData = GetApiAccessData();

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiData.apiUrl + "Case/GetAllCases?IsRestrict=false");
                request.Method = "GET";
                var accessToken = GetAccessToken();
                request.Headers.Add("Authorization", "Bearer " + accessToken);
                WebResponse response = request.GetResponse();
                using (Stream webStream = response.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            appdata = JsonConvert.DeserializeObject<GameListData>(responseReader.ReadToEnd());
                        }
                    }
                }
                var acteeChangeGames = appdata.Data.Where(x => x.ModuleName == "ActeeChange").ToList();
                var acteeCommunicationGames = appdata.Data.Where(x => x.ModuleName == "ActeeCommunication").ToList();
                var acteeStyletradeGames = appdata.Data.Where(x => x.ModuleName == "Styletrade").ToList();
                var acteeLeadershipGames = appdata.Data.Where(x => x.ModuleName == "ActeeLeadership").ToList();

                HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(apiData.apiUrl + "Admin/GetAllTheory");
                request1.Method = "GET";
                request1.Headers.Add("Authorization", "Bearer " + accessToken);
                WebResponse response1 = request1.GetResponse();
                using (Stream webStream = response1.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            theorydata = JsonConvert.DeserializeObject<GameListData>(responseReader.ReadToEnd());
                        }
                    }
                }

                List<IContent> AlreadyAdded = new List<IContent>();
                long totalRecords;
                var contentService = Current.Services.ContentService;
                var gameCategoryPage = contentService.GetPagedChildren(nodeId, 0, 1000, out totalRecords).Where(x => x.Published);
                foreach (var item in gameCategoryPage)
                {
                    long itemChildrens;
                    var list = contentService.GetPagedChildren(item.Id, 0, 1000, out itemChildrens).ToList();
                    AlreadyAdded.AddRange(list);
                }

                foreach (GameData item in appdata.Data)
                {
                    if (item.Visible == true)
                    {
                        IContent isalready = AlreadyAdded.Where(a => !string.IsNullOrEmpty(a.GetValue<string>("uid", "de-DE")) && a.GetValue<string>("uid", "de-DE").Equals(item.Id)).FirstOrDefault();
                        var page_id = 0;
                        if (isalready != null)
                        {
                            try
                            {
                                page_id = isalready.Id;
                                UpdateGameinBackend(item, page_id, theorydata, umbracoContextFactory);
                            }
                            catch (Exception ex1)
                            {

                                throw new Exception(ex1.Message + " Error 1 " + page_id);
                            }
                        }
                        else
                        {
                            try
                            {
                                if (item != null && theorydata != null && nodeId > 0)
                                {

                                    page_id = AddGameinBackend(item, nodeId, theorydata, umbracoContextFactory);
                                }

                            }
                            catch (Exception ex1)
                            {

                                throw new Exception(ex1 + " Error 2");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return appdata;
        }
        public static GameQuizList GetAndAddAllGamesinBackendForQuiz(int nodeId, IContentService _contentService, IUmbracoContextFactory umbracoContextFactory)
        {
            GameQuizList appdata = new GameQuizList();
            GameListData theorydata = new GameListData();
            APIAccessData apiData = GetApiAccessData();

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiData.apiUrl + "/Quiz/GetAllQuizs?UserId=ec960524-f6b0-4668-abb1-9fb180d546a8&IsPlayed=false");
                request.Method = "GET";
                var accessToken = GetAccessToken();
                request.Headers.Add("Authorization", "Bearer " + accessToken);
                WebResponse response = request.GetResponse();
                using (Stream webStream = response.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            appdata = JsonConvert.DeserializeObject<GameQuizList>(responseReader.ReadToEnd());
                        }
                    }
                }
                List<IContent> AlreadyAdded = new List<IContent>();
                long totalRecords;
                var contentService = Current.Services.ContentService;
                var gameCategoryPage = contentService.GetPagedChildren(nodeId, 0, 1000, out totalRecords).Where(x => x.Published);
                foreach (var item in gameCategoryPage)
                {
                    long itemChildrens;
                    if (item.GetValue<string>("category","en-Us").ToLower().Equals("quiz"))
                    {
                        var list = contentService.GetPagedChildren(item.Id, 0, 1000, out itemChildrens).ToList();
                        AlreadyAdded.AddRange(list);
                    }                   
                }
                foreach (Quiz item in appdata.Data)
                {
                    IContent isalready = AlreadyAdded.Where(a => !string.IsNullOrEmpty(a.GetValue<string>("uid", "en-Us")) &&  a.GetValue<string>("uid", "en-Us").Equals(item.Id)).FirstOrDefault();
                    var page_id = 0;
                    if (isalready != null)
                    {
                        try
                        {
                            page_id = isalready.Id;
                            UpdateGameinBackendForQuiz(item, page_id, umbracoContextFactory);
                        }
                        catch (Exception ex1)
                        {

                            throw new Exception(ex1.Message + " Error 1 " + page_id);
                        }
                    }
                    else
                    {
                        try
                        {
                            if (item != null && theorydata != null && nodeId > 0)
                            {

                                page_id = AddGameinBackendForQuiz(item, nodeId, umbracoContextFactory);
                            }

                        }
                        catch (Exception ex1)
                        {

                            throw new Exception(ex1 + " Error 2");
                        }
                    }
                  
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return appdata;
        }

        /// <summary>
        /// This method is for get and add all styletrade game
        /// </summary>
        /// <param name="nodeId"></param>
        /// <param name="_contentService"></param>
        /// <param name="umbracoContextFactory"></param>
        /// <returns></returns>
        public static GameQuizList GetAndAddAllGamesinBackendForStyletrade(int nodeId, IContentService _contentService, IUmbracoContextFactory umbracoContextFactory)
        {
            GameQuizList appdata = new GameQuizList();
            GameListData theorydata = new GameListData();
            APIAccessData apiData = GetApiAccessData();
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiData.apiUrl + "/StyleTrade/GetAllStyleTradeSets?UserId=ec960524-f6b0-4668-abb1-9fb180d546a8&IsPlayed=false");
                request.Method = "GET";
                var accessToken = GetAccessToken();
                request.Headers.Add("Authorization", "Bearer " + accessToken);
                WebResponse response = request.GetResponse();
                using (Stream webStream = response.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            appdata = JsonConvert.DeserializeObject<GameQuizList>(responseReader.ReadToEnd());
                        }
                    }
                }
                List<IContent> AlreadyAdded = new List<IContent>();
                long totalRecords;
                var contentService = Current.Services.ContentService;
                var gameCategoryPage = contentService.GetPagedChildren(nodeId, 0, 1000, out totalRecords).Where(x => x.Published);
                foreach (var item in gameCategoryPage)
                {
                    long itemChildrens;
                    if (item.GetValue<string>("category", "en-Us").ToLower().Equals("Styletrade"))
                    {
                        var list = contentService.GetPagedChildren(item.Id, 0, 1000, out itemChildrens).ToList();
                        AlreadyAdded.AddRange(list);
                    }
                }

                foreach (Quiz item in appdata.Data)
                {
                    IContent isalready = AlreadyAdded.Where(a => !string.IsNullOrEmpty(a.GetValue<string>("uid", "en-Us")) && a.GetValue<string>("uid", "en-Us").Equals(item.Id)).FirstOrDefault();
                    var page_id = 0;
                    if (isalready != null)
                    {
                        try
                        {
                            page_id = isalready.Id;
                            UpdateGameinBackendForStyletrade(item, page_id, umbracoContextFactory);
                        }
                        catch (Exception ex1)
                        {

                            throw new Exception(ex1.Message + " Error 1 " + page_id);
                        }
                    }
                    else
                    {
                        try
                        {
                            if (item != null && theorydata != null && nodeId > 0)
                            {

                                page_id = AddGameinBackendForStyletrade(item, nodeId, umbracoContextFactory);
                            }

                        }
                        catch (Exception ex1)
                        {

                            throw new Exception(ex1 + " Error 2");
                        }
                    }
                   
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return appdata;
        }
        public static List<Actee.Web.Models.ApiModels.Package> GetAllUpodiSubscriptions(this IPublishedContent node)
        {
            List<Package> upodiProductPlans = new List<Package>();

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api-alpha.actee.com/Package/GetAllPackages");
                request.Method = "GET";
                request.ContentType = "application/json";
                WebResponse response = request.GetResponse();
                using (Stream webStream = response.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            upodiProductPlans = JsonConvert.DeserializeObject<RootObject>(responseReader.ReadToEnd()).Data.Where(x => x.ProductPlanId != Guid.Empty.ToString()).ToList();

                            PriceController priceController = new PriceController();
                            int result = priceController.AddUpdatePrice(upodiProductPlans);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return upodiProductPlans;
        }

        public static List<Actee.Web.Models.ApiModels.Package> SetPriceInBackend()
        {
            List<Package> packages = new List<Package>();

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api-alpha.actee.com/Package/GetAllPackages");
                request.Method = "GET";
                request.ContentType = "application/json";
                WebResponse response = request.GetResponse();
                using (Stream webStream = response.GetResponseStream())
                {
                    if (webStream != null)
                    {
                        using (StreamReader responseReader = new StreamReader(webStream))
                        {
                            packages = JsonConvert.DeserializeObject<RootObject>(responseReader.ReadToEnd()).Data.Where(x => x.ProductPlanId != Guid.Empty.ToString()).ToList();

                            PriceController priceController = new PriceController();
                            int result = priceController.AddUpdatePrice(packages);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return packages;
        }
    }
}