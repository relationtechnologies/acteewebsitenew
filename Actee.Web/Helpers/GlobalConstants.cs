﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Actee.Web.Helpers
{
    public class GlobalConstants
    {
        public const string FallbackCultureName = "en-Us";

        public static class DocumentTypeAlias
        {
            public const string Homepage = "home";
            public const string Navigation = "navigation";
            public const string GlobleSetting = "globalSettings";
            public const string BlogPost = "blogPost";
            public const string Clients = "clients";
            public const string ModelPopup = "modelPopup";
            public const string WebsiteConfiguration = "settingFolder";
            public const string Courses = "courses";
            public const string Employee = "employee";
            public const string Tools = "tools";
            public const string VideoBlogPost = "videoBlogPost";
        }
    }
}