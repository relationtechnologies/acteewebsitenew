﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Actee.Web.Models
{
    public class ApiModels
    {
        public class Result
        {
            public object data { get; set; }
            public bool success { get; set; }
            public string Message { get; set; }
        }
        public class CourseList
        {
            public Guid Id { get; set; }
            public string CourseName { get; set; }
            public string StartDate { get; set; }
            public string Duration { get; set; }
            public string LanguageType { get; set; }
            public int TotalSignupUser { get; set; }
        }
        public class Course
        {
            public Guid Id { get; set; }
            public string NodeName { get; set; }
            public string MetaTitle { get; set; }
            public string MetaKeywords { get; set; }
            public string MetaDescription { get; set; }
            public string BannerHeading { get; set; }
            public string BannerDescription { get; set; }
            public string BannerImage { get; set; }
            public string BannerImagePath { get; set; }
            public string UserName { get; set; }
            public string UserEmail { get; set; }
            public string Title { get; set; }
            public string CourseDescription { get; set; }
            public string CoursePracticalInformation { get; set; }
            public string CoursePrice { get; set; }
            public string CourseName { get; set; }
            public string StartDate { get; set; }
            public string Duration { get; set; }
            public string LanguageType { get; set; }
            public string Location { get; set; }
            public string Price { get; set; }
            public string IconImage { get; set; }
            public string IconImagePath { get; set; }
            public int TotalSignupUser { get; set; }

        }
        public class User
        {
            public Guid Id { get; set; }
            public string UserName { get; set; }
            public string Course { get; set; }
            public string Email { get; set; }
            public string Message { get; set; }
        }
        public class GameListData
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            public List<GameData> Data { get; set; }
        }
        public class GameQuizList
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            public List<Quiz> Data { get; set; }
        }
        public class TokenDetail
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public int expires_in { get; set; }
        }

        public class TokenData
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            public TokenDetail Data { get; set; }
            public object SessionID { get; set; }
            public object sessionName { get; set; }
        }
        public class APIAccessData
        {
            public string apiUrl { get; set; }
        }
        public class NameData
        {
            public string code { get; set; }
            public string title { get; set; }
        }
        public class LanguageData
        {
            public string name { get; set; }
            public string code { get; set; }
            public string state { get; set; }
        }
        public class Quiz
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public int AddedQuestions { get; set; }
            public int MaxQuestions { get; set; }
            public bool IsActive { get; set; }
            public string PlatformId { get; set; }
            public string Language { get; set; }
            public string Picture { get; set; }
            public int TicketId { get; set; }
            public string SessionBuilderId { get; set; }
            public string SessionTypeId { get; set; }
            public bool IsMultiplayer { get; set; }
            public bool IsComplete { get; set; }
            public string CreatedOn { get; set; }
            public object LastPlayed { get; set; }
            public string GuidePdfFiles { get; set; }
            public bool IsCommunity { get; set; }
            public string WebsiteDescription { get; set; }
            public string CopiedCaseFrom { get; set; }
            public string Length { get; set; }
            public int TimesPlayed { get; set; }
            public int LastWeekPlayed { get; set; }
            public string UserName { get; set; }
            public string UserEmail { get; set; }
            public string UserProfile { get; set; }
            public string UserId { get; set; }
            public int CreatedInDays { get; set; }
            public string FullDescription { get; set; }

        }
        public class GameData
        {
            public string Id { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public string CharacterSetName { get; set; }
            public string ModuleName { get; set; }
            public string POEditorProjectId { get; set; }
            public string POEditorCaseTitle { get; set; }
            public bool IsDemo { get; set; }
            public bool IsShortVersionAvailable { get; set; }
            public string Picture { get; set; }
            public string WebsiteDescription { get; set; }
            public bool Visible { get; set; }
            public string Length { get; set; }
            public List<string> ChoosenTheory { get; set; }
            public string Language { get; set; }
            public bool IsLocked { get; set; }
            public string LanguageCode { get; set; }
            public string UserName { get; set; }
            public string UserProfile { get; set; }
            public int TimesPlayed { get; set; }
        }
        public class GameLanguageData
        {
            public string name { get; set; }
            public string code { get; set; }
            public string state { get; set; }
        }
        public class GamePOEditorCaseTitleData
        {
            public string title { get; set; }
            public string code { get; set; }
        }

        public class UnitAmountInCents
        {
            public dynamic EUR { get; set; }

        }

        public class SubscriptionData
        {
            public string ProductPlanId { get; set; }
            public string FullName { get; set; }
            public string SKU { get; set; }
            public string CurrencyCode { get; set; }
            public decimal Price { get; set; }
            public string BillingPeriodInterval { get; set; }

        }

        //public class Subscription
        //{
        //    public List<SubscriptionData> Data { get; set; }
        //}

        public class Subscription
        {
            public List<UpodiProductPlan> UpodiProductPlan { get; set; }
            public List<BulkPlan> BulkPlan { get; set; }
        }

        //public class PlanData
        //{
        //    public Subscription Data { get; set; }
        //}

        public class PackageFeature
        {
            public string Id { get; set; }
            public int Version { get; set; }
            public string PackageId { get; set; }
            public string ActeeFeatureId { get; set; }
            public bool IsAllCases { get; set; }
            public bool Status { get; set; }
            public bool IsAdd { get; set; }
            public bool IsAdded { get; set; }
            public string Name { get; set; }
            public string Feature { get; set; }
        }

        public class PackageFeatureCases
        {
            public string Id { get; set; }
            public int Version { get; set; }
            public string packageFeatureId { get; set; }
            public string CaseId { get; set; }
            public bool Status { get; set; }
            public bool IsAdd { get; set; }
            public bool IsAdded { get; set; }
            public string Name { get; set; }
        }
        public class PackageReedemCodes
        {
            public int Discount { get; set; }
            public string DiscountCodeId { get; set; }
            public object DiscountDescription { get; set; }
            public object DiscountName { get; set; }
            public string Id { get; set; }
            public string Name { get; set; }
            public string PackageId { get; set; }
            public object Prefix { get; set; }
            public string ReedemCode { get; set; }
            public string UsedDate { get; set; }
            public string UserId { get; set; }
            public string Username { get; set; }
            public int Version { get; set; }
        }
        public class Package
        {
            public string Id { get; set; }
            public int Version { get; set; }
            public string PackageName { get; set; }
            public string Description { get; set; }
            public string Type { get; set; }
            public int LicenseQuantity { get; set; }
            public int SessionQuantity { get; set; }
            public double NetAmount { get; set; }
            public string Title { get; set; }
            public string Subtitle { get; set; }
            public string TypeText { get; set; }
            public bool IsGenericPackage { get; set; }
            public int NoOfDays { get; set; }
            public bool IsActive { get; set; }
            public double? PerMonthAmount { get; set; }
            public double? PerYearAmount { get; set; }
            public string PackageType { get; set; }
            public string ProductPlanId { get; set; }
            public string FullName { get; set; }
            public string SKU { get; set; }
            public string CurrencyCode { get; set; }
            public double Price { get; set; }
            public int BillingPeriodInterval { get; set; }
            public bool FromUpodi { get; set; }
            public int NoOfDuration { get; set; }
            public List<PackageFeature> packageFeature { get; set; }
            public List<PackageFeatureCases> packageFeatureCases { get; set; }
            public List<object> packageReedemCodes { get; set; }
        }
        public class UpodiProductPlan
        {
            public string ProductPlanId { get; set; }
            public string FullName { get; set; }
            public string SKU { get; set; }
            public string CurrencyCode { get; set; }
            public decimal Price { get; set; }
            public int BillingPeriodInterval { get; set; }
            public string TypeText { get; set; }
        }

        public class BulkPlan
        {
            public string Id { get; set; }
            public string BulkPlanName { get; set; }
            public string Description { get; set; }
            public string Type { get; set; }
            public int DiscountPercentage { get; set; }
            public int BaseAmount { get; set; }
            public int LicenseQuantity { get; set; }
            public int NetAmount { get; set; }
            public string Title { get; set; }
            public string SubTitle { get; set; }
            public int Amount { get; set; }
            public string TypeText { get; set; }
        }

        public class Data
        {
            public List<UpodiProductPlan> UpodiProductPlan { get; set; }
            public List<BulkPlan> BulkPlan { get; set; }
        }

        public class RootObject
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            public List<Package> Data { get; set; }
            public object SessionID { get; set; }
        }
    }
}