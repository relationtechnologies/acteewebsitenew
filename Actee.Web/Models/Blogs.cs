﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace Actee.Web.Models
{
    public class Blogs
    {
        public List<IPublishedContent> blogposts { get; set; }
    }
}