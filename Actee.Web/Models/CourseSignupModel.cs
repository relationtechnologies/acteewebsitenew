﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Actee.Web.Models
{
    public class CourseSignupModel
    {
        public string Name { set; get; }

        public string Email { set; get; }

        public string course { set; get; }
        public string Message { set; get; }

        public string modelID { get; set; }
    }
}