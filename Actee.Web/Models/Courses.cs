﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace Actee.Web.Models
{

    public class Courses
    {
        public List<IPublishedContent> course { get; set; }
    }
    public class Client
    {
        public List<IPublishedContent> clients { get; set; }
    }

}