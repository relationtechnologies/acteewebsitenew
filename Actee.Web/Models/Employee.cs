﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Actee.Web.Models
{
    public class Employee
    {
        public string ActionType { get; set; }
        public int NodeId { get; set; }
        public string EmployeeName { get; set; }
        public string Desgination { get; set; }
        public string ContactNumber { get; set; }
        public string EmployeeSmallImage { get; set; }
        public string EmployeeDescription { get; set; }
        public List<string> EmployeeType { get; set; }
        public List<SocialMedia> SocialMediaList { get; set; }
        public List<Country> CountriesOfOperation { get; set; }
        public string ConsultantType { get; set; }
        public string Language { get; set; }
        public string Skills { get; set; }
        public string DateOfCertification { get; set; }
        public string PlaceOfCertification { get; set; }
        public string LanguageOfCertification { get; set; }
        public string WebsiteName { get; set; }
        public string InstructorName { get; set; }
        public List<Concepts> ConceptCertifiedIn { get; set; }
        public bool isCertifier { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

    }

    public class Concepts
    {
        public string value { get; set; }
        public string name { get; set; }
        public string instructorName { get; set; }
        public string certificationDate { get; set; }
        public string placeOfCertification { get; set; }
        public string approvedDate { get; set; }
        public bool IsApproved { get; set; }
    }

    public class SocialMedia
    {
        public string Name { get; set; }
        public string IconHeading { get; set; }
        public string IconLink { get; set; }
        public string IconDescription { get; set; }
        public string IconImage { get; set; }
    }

    public class Country
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class RootObjects
    {
        public Guid key { get; set; }
        public string name { get; set; }
        public string ncContentTypeAlias { get; set; }
        public string socialMediaName { get; set; }
        public string iconImage { get; set; }
        public string iconLink { get; set; }
        public string iconHeading { get; set; }
        public string iconDescription { get; set; }
    }

    public class PartnerHubModel
    {
        public string Name { get; set; }
        public string Logo { get; set; }
        public string Description { get; set; }
        public string ActionType { get; set; }
        public int NodeId { get; set; }
        public string LanguageCode { get; set; }
        public string WebsiteName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Language { get; set; }
        public string Desgination { get; set; }
    }
}