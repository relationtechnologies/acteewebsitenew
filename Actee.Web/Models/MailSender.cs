﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace Actee.Web.Models
{
    public class MailSender
    {
        string MailSenderUserName = WebConfigurationManager.AppSettings["MailSenderUserName"].ToString();
        string MailSenderDisplayName = WebConfigurationManager.AppSettings["MailSenderDisplayName"].ToString();
        string MailSenderPass = WebConfigurationManager.AppSettings["MailSenderPass"].ToString();
        string WebsiteUrl = WebConfigurationManager.AppSettings["WebsiteUrl"].ToString();
        string SiteLogUrl = WebConfigurationManager.AppSettings["SiteLogUrl"].ToString();
        string WebSiteName = WebConfigurationManager.AppSettings["WebSiteName"].ToString();
        string contactPersonEmail = WebConfigurationManager.AppSettings["contactPersonEmail"].ToString();
        string MailHost = WebConfigurationManager.AppSettings["MailHost"].ToString();
        int PortNumber = Convert.ToInt32(WebConfigurationManager.AppSettings["PortNumber"].ToString());
        bool EnableSsl = Convert.ToBoolean(WebConfigurationManager.AppSettings["EnableSsl"].ToString());

        public bool sendMail(string to, string html, string subject, System.IO.Stream attach, string fileName)
        {
            bool retVal;
            try
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                SmtpClient smtpClient = new SmtpClient();
                msg.From = new System.Net.Mail.MailAddress(MailSenderUserName, MailSenderDisplayName);
                msg.To.Add(to);
                msg.Subject = subject;
                msg.Body = html;
                msg.Attachments.Add(new Attachment(attach, fileName));
                msg.IsBodyHtml = true;
                smtpClient.Host = MailHost;
                smtpClient.Credentials = new System.Net.NetworkCredential(MailSenderUserName, MailSenderPass);
                smtpClient.EnableSsl = EnableSsl;
                smtpClient.Port = PortNumber;

                smtpClient.Send(msg);
                retVal = true;

            }
            catch (Exception ee)
            {
                throw ee;
            }

            return retVal;
        }

        public bool sendMail(string to, string html, string subject)
        {
            bool retVal;
            try
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                SmtpClient smtpClient = new SmtpClient();
                msg.From = new System.Net.Mail.MailAddress(MailSenderUserName, MailSenderDisplayName);
                msg.To.Add(to);
                msg.Subject = subject;
                msg.Body = html;

                msg.IsBodyHtml = true;
                smtpClient.Host = MailHost;
                smtpClient.Credentials = new System.Net.NetworkCredential(MailSenderUserName, MailSenderPass);
                smtpClient.EnableSsl = EnableSsl;
                smtpClient.Port = PortNumber;

                smtpClient.Send(msg);
                retVal = true;

            }
            catch (Exception ee)
            {
                throw ee;
            }

            return retVal;
        }

        public bool sendMail(string to, string html, string subject, System.Net.Mail.MailMessage msg)
        {
            bool retVal;
            try
            {
                //System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                SmtpClient smtpClient = new SmtpClient();
                msg.From = new System.Net.Mail.MailAddress(MailSenderUserName, MailSenderDisplayName);
                msg.To.Add(to);

                msg.Subject = subject;
                msg.Body = html;

                msg.IsBodyHtml = true;
                smtpClient.Host = MailHost;
                smtpClient.Credentials = new System.Net.NetworkCredential(MailSenderUserName, MailSenderPass);
                smtpClient.EnableSsl = EnableSsl;
                smtpClient.Port = PortNumber;

                smtpClient.Send(msg);
                retVal = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }

        internal bool sendMail(object contactpersonEmail, string finalHtml, object subject)
        {
            throw new NotImplementedException();
        }

        public bool sendMail(string to, MailAddressCollection bccEmails, string html, string subject, System.Net.Mail.MailMessage msg)
        {
            bool retVal;
            try
            {
                //System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                SmtpClient smtpClient = new SmtpClient();
                msg.From = new System.Net.Mail.MailAddress(MailSenderUserName, MailSenderDisplayName);
                msg.To.Add(to);
                foreach (MailAddress addr in bccEmails)
                {
                    msg.Bcc.Add(addr);
                }
                msg.Subject = subject;
                msg.Body = html;

                msg.IsBodyHtml = true;
                smtpClient.Host = MailHost;
                smtpClient.Credentials = new System.Net.NetworkCredential(MailSenderUserName, MailSenderPass);
                smtpClient.EnableSsl = EnableSsl;
                smtpClient.Port = PortNumber;

                smtpClient.Send(msg);
                retVal = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return retVal;
        }
        public bool sendMail(string to, MailAddressCollection bccEmails, string html, string subject)
        {
            bool retVal;
            try
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                SmtpClient smtpClient = new SmtpClient();
                msg.From = new System.Net.Mail.MailAddress(MailSenderUserName, MailSenderDisplayName);
                msg.To.Add(to);
                foreach (MailAddress addr in bccEmails)
                {
                    msg.Bcc.Add(addr);
                }
                msg.Subject = subject;
                msg.Body = html;

                msg.IsBodyHtml = true;
                smtpClient.Host = MailHost;

                smtpClient.Credentials = new System.Net.NetworkCredential(MailSenderUserName, MailSenderPass);
                smtpClient.EnableSsl = EnableSsl;
                smtpClient.Port = PortNumber;

                smtpClient.Send(msg);
                retVal = true;

            }
            catch (Exception ee)
            {
                throw ee;
                // retVal = false;
            }

            return retVal;
        }

        public string getMailHTML(string EmailTitle, string UserName, string Message)
        {
            StringBuilder mailHtml = new StringBuilder("");
            mailHtml.Append("<div align='center'><div style='width: 650px'><div style=' width: 650px; color: #fff; background-color: #001b33; border-radius: 5px 5px 0px 0px;");
            mailHtml.Append("font-size: 11px; font-family: tahoma,verdana,arial,sans-serif'><div style='padding: 2px 29px;text-align: left;color: #FAFAFA;'>");
            mailHtml.Append("<h2 style='font-family: tahoma,verdana,arial,sans-serif;color: #FED20B;'>" + WebSiteName + "</h2>");
            mailHtml.Append("</div></div><div><table cellpadding='0' cellspacing='5px' style='border: 2px solid #a6a6a6; border-width: 0 2px;");
            mailHtml.Append(" width: 650px; font-size: 13px; background-color: #dddddd'><tbody><tr style='background-color: #ffffff'>");
            mailHtml.Append("<td><table cellpadding='0' cellspacing='0' width='100%'><tbody><tr>");
            mailHtml.Append("<td style='vertical-align: top;background-color:white;'><a href='" + WebsiteUrl + "' target='_blank'>");
            mailHtml.Append("<img src='" + SiteLogUrl + "' style='padding: 20px 20px;height: 115px; width: 130px; border: 0px'></a>");
            mailHtml.Append("</td><td style='vertical-align: middle; padding-left: 20px; padding-right: 25px; padding-top: 20px;text-align: left'>");
            mailHtml.Append("<div style='background-color: #ffffff; color: #333333; text-transform: uppercase;");
            mailHtml.Append("font-size: 16px; font-weight: bold; margin-bottom: 10px'>" + EmailTitle + "</div><br>");
            mailHtml.Append("<div style='font-family: tahoma,verdana,arial,sans-serif; font-size: 13px; margin-bottom: 15px'>Hello " + UserName + "</div>");
            mailHtml.Append("<div style='font-size: 13px; font-family: tahoma,verdana,arial,sans-serif; margin-bottom: 15px'>");
            mailHtml.Append("" + Message + "</div>");
            mailHtml.Append("<div style='text-align: center; padding: 15px 0; margin: 10px 0; font-family: tahoma,verdana,arial,sans-serif'>");
            mailHtml.Append("<a style='background-color: #FF7906; color: #ffffff; font-size: 14px; font-weight: bold;");
            mailHtml.Append("  padding: 5px 10px; border-radius: 3px; text-decoration: none' href='" + WebsiteUrl + "' target='_blank'>");
            mailHtml.Append("    Go To " + WebSiteName + "</a>");
            mailHtml.Append(" </div></td></tr></tbody></table></td></tr></tbody></table></div></div>");
            mailHtml.Append(" <div style='width: 650px; color: #fff; background-color: #001b33; border-radius: 0 0 5px 5px;");
            mailHtml.Append(" font-size: 11px; font-family: tahoma,verdana,arial,sans-serif'>");
            mailHtml.Append(" <div style='padding: 15px 0'>Thank You : <a style='color: #FED20B;' href='" + WebsiteUrl + "' target='_blank'>" + WebSiteName + "</a>");
            mailHtml.Append(" </div></div></div>");

            return mailHtml.ToString();
        }
    }
}