﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Actee.Web.Models
{
    public class RelatedLink
    {
        public string Caption { get; set; }
        public string Link { get; set; }
        public bool NewWindow { get; set; }
        public int? Internal { get; set; }
        public bool IsInternal { get; set; }
        public string InternalName { get; set; }
        public string Title { get; set; }
    }
}